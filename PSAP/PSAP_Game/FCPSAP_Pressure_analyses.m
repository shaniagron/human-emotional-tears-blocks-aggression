%% Press Analysis
%loading the raw signal of the presses
cd ('/Users/eva/Box/PSAP_PTB/PSAP_PTB_exp')
%create diary
Diaryfilename = sprintf('FCPSAP_Pressure_analyses_%s.txt', datetime('now'));
diary Diaryfilename
diary on
warning('off','MATLAB:colon:nonIntegerIndex')


loadPSAPdata_all

%% filter
%apply all sorts of filters to the signal
FilterData = 0; %if I want filtered data, switch this to 1. 

filterPSAPdata %movemean and filtered data (depends on FilterData)
%% use MATLAB data to replace bad signal

fs = 100;
whichone = 1;
downsampledLC = downsample(Subject_info(4).PressureDATA.rawLC,10);
p = pspectrum(downsampledLC(85131:102663,2),fs);
[pxx,f] = pspectrum(downsampledLC(85131:102663,2),fs); %2 is moisy
[pxx1,f1] = pspectrum(downsampledLC(85131:102663,1),fs);
plot(f,pow2db(pxx)); hold on; plot(f1,pow2db(pxx1));
legend('first','second')
plot(p)
legend('first','second')
plot(downsampledLC(:,whichone))

p1 = pspectrum(zscore(downsampledLC(85131:102663,2)),fs,'spectrogram', ...
    'TimeResolution',2.56,'Overlap',86,'Leakage',0.875);
p2 = pspectrum(zscore(downsampledLC(85131:102663,1)),fs,'spectrogram', ...
    'TimeResolution',2.56,'Overlap',86,'Leakage',0.875);
 
ax = gca
[c,lags] = xcorr(zscore(downsampledLC(:,whichone)),zscore(Subject_info(4).PressureDATA.rawM{:,2}));
figure, %stem(lags,c)
c = c/max(c);
 [M,I] = max(c);
 tlag = lags(I);
plot(lags,c,[tlag tlag],[-0.5 1],'r:')
Lag = lags(find(c==max(c)))
shifted = circshift(Subject_info(4).PressureDATA.rawM{:,2},Lag);

figure, plot(shifted+1.5);hold on; plot(downsampledLC(:,whichone))

%% loading subject events protocols
%loading the matlab output of the experiment, with full events, times and
%states
loadSubjectEventsProtocol


%% check if right or left

determineLorRpress


%% create inclusion vector
for fileLoop = 1:length(Subject_info)
   Subject_info(fileLoop).Inclusion = ~isempty(Subject_info(fileLoop).events);
end

fprintf('%d out of %d will be included in further analyses\n',sum([Subject_info(:).Inclusion]),length([Subject_info(:).Inclusion]))

%% Normalize according to calibration?
perc = 0.75;
plot_subsets

normalizingPressesToCalibration

% figure
% for fileLoop = find([Subject_info(:).Inclusion])
%     %if Subject_info(fileLoop).decisions.toPlotOrNotToPlot
%         subplot(1,2,1); hold on
%         histogram(Subject_info(fileLoop).PressureDATA.Rpress_normalized,'EdgeColor','none','FaceAlpha' ,0.2)
%         subplot(1,2,2); hold on
%         histogram(Subject_info(fileLoop).PressureDATA.Lpress_normalized,'EdgeColor','none', 'FaceAlpha' ,0.2)
%     %end
% end

%loop for each type of event, take 3 sec after, 2 after and look for peaks,
%save max height, and time of rise (d') and decrease. maybe save max slope,
%for both.
%create table containing start, end, event, trapz, max slop up, max slope down, and condition. create protocols

%% Flip handness if needed for a predefined vector
Vec_o_Flippin = [28 40 8];
for fileLoop = Vec_o_Flippin
    [Subject_info(fileLoop).PressureDATA.Lpress,...
        Subject_info(fileLoop).PressureDATA.Rpress,...
        Subject_info(fileLoop).PressureDATA.Handness,...
        Subject_info(fileLoop).PressureDATA.Lpress_z,...
        Subject_info(fileLoop).PressureDATA.Rpress_z]...
        = FlipHandness(Subject_info(fileLoop).PressureDATA.Lpress, ...
                       Subject_info(fileLoop).PressureDATA.Rpress,...
                       Subject_info(fileLoop).PressureDATA.Handness,...
                       Subject_info(fileLoop).PressureDATA.Lpress_z,...
                       Subject_info(fileLoop).PressureDATA.Rpress_z);
                   
    fprintf('Flipped subject %s\n', Subject_info(fileLoop).subjectID)
end

%% finding slopes and what not per event

%loading the indices and times of events during the task
loadSubjectTaskEvents

%% some more exclusion
for fileLoop = [19 55 56 36 5 ]%count out 5 next time, I switched the data
   Subject_info(fileLoop).Inclusion = 0;
end

fprintf('%d out of %d will be included in further analyses\n',sum([Subject_info(:).Inclusion]),length([Subject_info(:).Inclusion]))
%%
%snipOUTevents %for normalized data
%snipOUTevents_notNormalized %for not normalized data

snipOUTevents_Zscored %for zscored data - ****** I should copy everything I added here to the other snipOUTevents scripts!

plot_PressureRise_detection %plot a subset of pressure rise detection as done in "snipOUTevents_Zscored"

%% create seperate lists for monetray with prov and monetary without prov
figure
for fileLoop = find([Subject_info(:).Inclusion])
      hold on
    plot(Subject_info(fileLoop).EventRelatedPressure.events_provocation_buffered.PressureRZ,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1);
  %plot(Subject_info(fileLoop).events.TaskEvents.events_provocation_buffered,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1);
  title('events_provocation_buffered, R')
end
%%
figure
for fileLoop = find([Subject_info(:).Inclusion])
    scatter(1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ),Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ...
      , 10 , colors(1,:), 'filled', 'MarkerFaceAlpha', 0.5); hold on
end

for fileLoop = find([Subject_info(:).Inclusion])
    scatter(1:length(Subject_info(fileLoop).EventRelatedPressure.events_provocation.maxRZ),Subject_info(fileLoop).EventRelatedPressure.events_provocation.maxRZ,...
        10 , colors(3,:), 'filled', 'MarkerFaceAlpha', 0.5); hold on

end

for fileLoop = find([Subject_info(:).Inclusion])
    scatter(1:length(Subject_info(fileLoop).EventRelatedPressure.events_aggression.maxRZ),Subject_info(fileLoop).EventRelatedPressure.events_aggression.maxRZ,...
        10 , colors(6,:), 'filled', 'MarkerFaceAlpha', 0.5); hold on

end
legend('monetary', ' provocation', 'aggression')




%%
figure;
plot(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureRZ)
figure;
scatter(1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ),Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ); hold on
scatter(1:length(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation),Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ...
    (:,Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)));
scatter(1:length(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation)-1,Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ...
    (:,Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(1:end-1,1)+1));
scatter(1:length(Subject_info(fileLoop).EventRelatedPressure.events_aggression.maxRZ),Subject_info(fileLoop).EventRelatedPressure.events_aggression.maxRZ); hold on
scatter(1:length(Subject_info(fileLoop).EventRelatedPressure.events_aggression.maxLZ),Subject_info(fileLoop).EventRelatedPressure.events_aggression.maxLZ); hold on
scatter(1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxLZ),Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxLZ); hold on
legend('events_monetary', 'events_monetary_with provocation', 'events_monetary after provocation', 'events_aggression', 'events_aggression L', 'events_monetary L')
%%
PSAP_figs_nums

%% To do
%plot according to a vector that is seriali - what happened when and look
%at the dynamics in time and in relation to past events

%plot the pressure according to the type of event. 

% do a provocation analysis dependent of the participant stoppped or not?