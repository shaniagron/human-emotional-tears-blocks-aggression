%PSAP provocation
%% provocation!
            provcation_index = monetary_idx==provocation_vec;
            to_provoke = any(provcation_index);
            if to_provoke && ((GetSecs - sessionTime) > wait_for_provocation(provcation_index))
                wait_for_provocation(provcation_index) = inf; % #ok<SAGROW> %only provoke once per session
                sprintf('provoking: monetary_idx = %d',monetary_idx);
                provocation_vec
                %%%%signal to lab chart%%%%
                %TriggerLabChart('provocation',olfactometerIP);
                %%%%%%%%%%%%%%%%%%%%%%%%%%%
                ProvocationTime = GetSecs;
                %Provocation
                amount = amount - provocation_deduction;
                Screen('TextSize', window, textSize);
                Screen('DrawText', window, num2str(amount), position(3)-centerize_amount,position(2)+300);
                Screen('FrameRect', window, [0 0 0] , baseRect ); %frame for square
                Screen('FrameRect', window, [0 0 0], baseRect2 ); %frame for square2
                Screen('FillRect', window, RedColor, baseRect+[0 frameSize+200-counter*stepsize 0 0] ); %fill dependent on the press;
                Screen('DrawText', window, num2str(-provocation_deduction), position(3)-centerize_amount,position(2)+400,RedColor);%draw provocation
                Screen('Flip',window);
                pause(1);
                records(ii,:) = WriteRecoeds(experimentStart,ProvocationTime,run_num, amount, 'provocation');
                records{ii,5}= ProvocationTime+1-experimentStart;
                ii=ii+1;
            end