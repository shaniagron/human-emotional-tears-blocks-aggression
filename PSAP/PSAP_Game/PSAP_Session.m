%script PSAP_Session
% Runs one PSAP session of mometary and revenge state
sessionLength = 120;%4*60;

waitTime = 4;
%perturbe every 60-120 secs
state = 'none';
writeState(state);
one = false;
two = false;
stepsize = 10;
times = 0;

while GetSecs < startTime+sessionLength
    fprintf('PSAP iteration\n');
    
    
    showImageNwait( CrossImage, window, windowRect, waitTime);
    %sendState(hardware,t,state);
    fprintf('sending state %s...\n',state);
    %Screen('Flip',window);
    Screen('TextSize', window, textSize);
    Screen('DrawText', window, num2str(amount), position(3),position(2)+200);%, [1 0 0]);
    Screen('FrameRect', window, [0 0 0], baseRect ); %frame for square
    Screen('FrameRect', window, [0 0 0], baseRect2 ); %frame for square 2
    Screen('Flip',window);
    % instruction
    sessionTime = GetSecs;
    
    if (~hardware)
        [secs,keyCode,deltaSecs] = KbWait([],[],GetSecs+4);
    else
        keyCode = 12;%'Clear', or in other words, not relevant.
    end
    while one || two || GetSecs-sessionTime<4 || KbCheck
        %fprintf('entering cases...\n');
        if one &&two && (strcmp(state,'none') || strcmp(state,'monetary')) ||(one &&two && (strcmp(state,'revenge') && GetSecs-sessionTime<2)) || strcmp(KbName(keyCode),'1!') %squeeze both
            %fprintf('entering monetary case...\n');
            %deadline = monetaryDeadline;
            counter=counter+1;
            Screen('TextSize', window, textSize);
            Screen('DrawText', window, num2str(amount), position(3),position(2)+200);%, [1 0 0]);
            Screen('FrameRect', window, [0 0 0], baseRect ); %frame for square
            Screen('FrameRect', window, [0 0 0], baseRect2 ); %frame for square2
            Screen('FillRect', window, [1 0 0], baseRect+[0 frameSize-counter*stepsize 0 0] ) %fill dependent on the press;
            Screen('Flip',window);
            if counter>=full %start over when counter is full
                counter=0;
                amount=amount+5;
                times = times+1;
                if (times==2)
                    break
                end
            end
            %prevstate = state;
            state = 'monetary';
            writeState(state);
            %if (~strcmp(prevstate, state))
            %    fprintf('prevstate: %s. new state: %s. sending state...\n',prevstate,state);
            %    sendState(hardware,t,state);
            %end
            
            if GetSecs > sessionTime+State.(state).deadline
                fprintf('breaking one and two\n');
                break
            end
            %elseif strcmp(KbName(keyCode),'2@') && (strcmp(state,'none') || strcmp(state,'revenge'))
        elseif xor(one,two) && (strcmp(state,'none') || strcmp(state,'revenge')) || (xor(one,two) &&strcmp(state,'monetary') && GetSecs-sessionTime<2) || strcmp(KbName(keyCode),'2@')%only one, not both
            %deadline=revengeDeadline;
            %fprintf('entering revenge case...\n');
            counter=counter+2;
            Screen('TextSize', window, textSize);
            Screen('DrawText', window, num2str(amount), position(3),position(2)+200);%, [1 0 0]);
            Screen('FrameRect', window, [0 0 0], baseRect ); %frame for square
            Screen('FrameRect', window, [0 0 0], baseRect2 ); %frame for square 2
            Screen('FillRect', window, [1 0 0], baseRect2+[0 frameSize-counter*stepsize 0 0] ) %fill dependent on the press;
            Screen('Flip',window);
            %prevstate = state;
            state = 'revenge';
            writeState(state);
            %if (~strcmp(prevstate, state))
            %    fprintf('prevstate: %s. new state: %s. sending state...\n',prevstate,state);
            %    sendState(hardware,t,state);
            %end
            if counter>=full %start over when counter is full
                counter=0;
                break;
            end
            if GetSecs > sessionTime+State.(state).deadline
                fprintf('breaking one or two %f\n',GetSecs-sessionTime);
                break
            end
            
            
        end
        if GetSecs > sessionTime+State.(state).deadline
                fprintf('breaking external %f\n',GetSecs-sessionTime);
                break
        end
        %pauseTime = GetSecs;
        %while GetSecs-pauseTime<0.25
        %    pause(0.05);
        %end
        pause(State.(state).steptime);
        times = 0;
    end
    %reset
    counter=0;
    fprintf('state = %s\n',state);
    records{i,1} = GetSecs-experimentStart;
    records{i,2} = amount;
    records{i,3}= string(state);
    i=i+1;
    save ('records.mat', 'records');
    state = 'none';
    writeState(state);
    one = false;
    two = false;
end
%end
records.Properties.VariableNames = {'startTime' 'amount' 'state'};

clear counter;
clear MEX %flushEvents; % or
%% data
%save data
%what data do i save?
%presses, time of presses,time of provocations, money count.
writetable(records,sprintf('%s/records1.csv',dataDirectory),'Delimiter',',');