
secs_buffer = 2000; %how long before and after should we look at the rise of the pressure
event_type = {'monetary', 'revenge', 'provocation'};
for fileLoop = 1:length(Subject_info)
    if ~isempty(Subject_info(fileLoop).events)
        INDmonetary = string(Subject_info(fileLoop).events.fullProtocol{:,3})=='monetary';
        INDaggression = string(Subject_info(fileLoop).events.fullProtocol{:,3})=='revenge';
        INDprovocation = string(Subject_info(fileLoop).events.fullProtocol{:,3})=='provocation';
        events_monetary = [Subject_info(fileLoop).events.fullProtocol{INDmonetary,6}-secs_buffer,...
            Subject_info(fileLoop).events.fullProtocol{INDmonetary,7}+3*secs_buffer];
        events_aggression = [Subject_info(fileLoop).events.fullProtocol{INDaggression,6}-secs_buffer,...
            Subject_info(fileLoop).events.fullProtocol{INDaggression,7}+secs_buffer];
        events_provocation = [Subject_info(fileLoop).events.fullProtocol{INDprovocation,6}, ...
            Subject_info(fileLoop).events.fullProtocol{INDprovocation,6}+1000]; %provocations are always 1 sec
        events_provocation_buffered = [Subject_info(fileLoop).events.fullProtocol{INDprovocation,6}-secs_buffer, ...
            Subject_info(fileLoop).events.fullProtocol{INDprovocation,6}+1000+secs_buffer]; %provocations are always 1 sec
        
        event_num = 1;
        for mm = 1:length(events_provocation)
            try
            %in which mon event did the prov took place
            whichevent = find(events_monetary(:,1)<events_provocation(mm,1) & events_provocation(mm,1)<events_monetary(:,2));
            [~,I] = min(whichevent-event_num);
            Provind(mm,1) = whichevent(I);
            Provind(mm,2) = events_provocation(mm,1)-events_monetary(Provind(mm,1),1);
            event_num = Provind(mm,1);
            catch
            end
        end
        
        Subject_info(fileLoop).events.TaskEvents.INDmonetary = INDmonetary;
        Subject_info(fileLoop).events.TaskEvents.INDaggression = INDaggression;
        Subject_info(fileLoop).events.TaskEvents.INDprovocation = INDprovocation;
        Subject_info(fileLoop).events.TaskEvents.events_monetary = events_monetary;
        Subject_info(fileLoop).events.TaskEvents.events_aggression = events_aggression;
        Subject_info(fileLoop).events.TaskEvents.events_provocation = events_provocation;
        Subject_info(fileLoop).events.TaskEvents.events_provocation_buffered = events_provocation_buffered;
        Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation = Provind;
        
        disp(['Loading events data for subject ' Subject_info(fileLoop).subjectID])
       
        
        clear INDmonetary; clear INDaggression; clear INDprovocation;
        clear events_monetary; clear events_aggression; clear events_provocation;
        clear Provind; clear mm; clear event_num;
        
    elseif isempty(Subject_info(fileLoop).events)
        disp(['Failed to load task events data for subject ' Subject_info(fileLoop).subjectID])
        fprintf('fileLoop = %d\n', fileLoop)
    else
        disp(['Something weird happened, please check subject ' Subject_info(fileLoop).subjectID])
    end
    
    
end