function [Subject_info] = randomize_2PlotorNot(perc,Subject_info)
% This function creates a subset at a requested ratio of plotted and not plotted subjects. 
% Useful for sampling part of the subjects and not all of them.
% Using the following code we can see what will be the approximate portion
% of the subjects this funftion will plot for each requested ratio.
%%%

%n=0;
% for perc = [0.2:0.1:1]
%     n=n+1;
%     for ii = 1:1000
%         
%         ransomized_plot_value(ii,n) = round(rand*perc);
%         
%     end
% end
% figure
% scatter([0.2:0.1:1], sum(ransomized_plot_value)/1000*100)
%ylabel('percnetage of plotted')
%xlabel('multiplication factor')


for fileLoop = 1:length(Subject_info)
    ransomized_plot_value(fileLoop) = round(rand*perc);
    Subject_info(fileLoop).decisions.toPlotOrNotToPlot = ransomized_plot_value(fileLoop);  

end
fprintf('~%2.0f percent of the subjects will be plotted\n',sum(ransomized_plot_value)/1000*100);

end

