
[window, windowRect] = PsychImaging('OpenWindow', screenNumber, gray); %open a gray screen
[screenXpixels, screenYpixels] = Screen('WindowSize', window); % Get the size of the on screen window
records = table;
i = 1;

waitTime = 1.2; textSize = 150;
xPos = windowRect(3)*.5; yPos = windowRect(4)*.73;
Screen('TextColor', window, RedColor);
Screen('TextSize', window, 100);

records{i,1} = GetSecs-experimentStart;
records{i,2} = NaN;
records{i,3}= string('pressTest');
i=i+1; save ('records.mat', 'records');
amount = 5;
    [xCenter, yCenter] = RectCenter(windowRect);
    position = [0 0 xCenter yCenter*1.5];
    %square x
    % Get the centre coordinate of the window
    % Make a base Rect of 200 by 200 pixels
    frameSize = 200;
    x1 = xCenter/2-frameSize/2;
    baseRect = [x1 yCenter x1+frameSize yCenter+frameSize];
    baseRect2 = baseRect+[xCenter 0 xCenter 0];
    counter=0;
 
experimentStart = GetSecs;   
    startTime = GetSecs;
    State.monetary.deadline = 10;
    State.revenge.deadline = 4;
    State.none.deadline = 10;
    full = 20;
    State.monetary.steptime = State.monetary.deadline/(2.5*full);
    State.none.steptime = State.monetary.steptime;
    State.revenge.steptime = State.revenge.deadline/full;
    records{i,1} = GetSecs-experimentStart;
    records{i,2} = NaN;
    records{i,3}= string('Round1');
    i=i+1;
    sessionLength = 120;%4*60;

waitTime = 4;
%perturbe every 60-120 secs
state = 'none';

one = false;
two = false;
stepsize = 10;
times = 0;
earning = 3; %how much money you earn with every full counter
centerize_amount = 50*(length(num2str(amount))-1); %centralize the amount

    
    fprintf('sending state %s...\n',state);
    Screen('Flip',window);
    Screen('TextSize', window, textSize);
    Screen('DrawText', window, num2str(amount), position(3)-centerize_amount,position(2)+250);
    Screen('FrameRect', window, [0 0 0], baseRect ); %frame for square
    Screen('FrameRect', window, [0 0 0], baseRect2 ); %frame for square 2
    Screen('Flip',window);
    
    WaitSecs(1000);
    sca
    