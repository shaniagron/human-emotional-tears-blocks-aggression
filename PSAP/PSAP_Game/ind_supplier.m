function [ind_event,events_buffered] = ind_supplier(event_type,subj_events,secs_buffer)
%UNTITLED4 Summary of this function goes here

if string(event_type)=='monetary' || string(event_type)=='revenge'
    %events of mon and revenge
    ind_event = string(subj_events{:,3})==event_type;
    events_buffered = [subj_events{ind_event,6}-secs_buffer, subj_events{ind_event,7}+secs_buffer];
elseif string(event_type)=='provocation'
    ind_event = string(subj_events{:,3})=='provocation';
    events_buffered = [subj_events{ind_event,6}, subj_events{ind_event,6}+1000]; %provocations are always 1 sec
end

