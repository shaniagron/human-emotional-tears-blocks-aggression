%check events per subject
% prompt = 'Which subject # do you want to check? ';
% fileLoop = input(prompt)

%fileLoop = [4 5 8 14 16 19 23 28 31 36 37 40 42 46 51 55 57] ;
%for fileLoop = find([Subject_info(:).Inclusion]) %fileLoop = [39 40 16 25 13 12 9 5] %
for fileLoop = 5%[36 19 8 5 4 9 3] 
    figure('Position', [10 400 1500 500])
    %subplot(1,2,1)
    h(1) = plot(Subject_info(fileLoop).PressureDATA.Lpress_z,'DisplayName','L'); hold on
    h(2) = plot(Subject_info(fileLoop).PressureDATA.Rpress_z,'DisplayName','R');
    
    hold on;
    event_types = {'events_strongL', 'events_lightL'};
    for eventos = 1:length(event_types)
        for iii = 1:size(Subject_info(fileLoop).events.calibration.(event_types{eventos}),1)
            event_plotted = Subject_info(fileLoop).events.calibration.(event_types{eventos})(iii,1):Subject_info(fileLoop).events.calibration.(event_types{eventos})(iii,2);
            scatter(event_plotted,zeros(1,length(event_plotted)), '.')
        end
    end
    
    annotation('textbox',[0.17 0.61 0.3 0.3],'String','0 = events L, 1 = events R', 'FitBoxToText','on')
    
    event_types = {'events_monetary', 'events_aggression', 'events_provocation'};
    color_events = [[0,167,249]/255;...%blue
                     [0 0 0];...%black
                     [122 122 122]/255];%gray for aggrssion
    for eventos = 1:length(event_types)
        for iii = 1:size(Subject_info(fileLoop).events.TaskEvents.(event_types{eventos}),1)
            event_plotted = Subject_info(fileLoop).events.TaskEvents.(event_types{eventos})(iii,1):...
                Subject_info(fileLoop).events.TaskEvents.(event_types{eventos})(iii,2);
            scatter(event_plotted,zeros(1,length(event_plotted)), [],color_events(eventos,:), '.')
        end
    end
    
    
    %subplot(1,2,2)
    hold on;
    event_types = {'events_strongR', 'events_lightR'};
    for eventos = 1:length(event_types)
        for iii = 1:size(Subject_info(fileLoop).events.calibration.(event_types{eventos}),1)
            event_plotted = Subject_info(fileLoop).events.calibration.(event_types{eventos})(iii,1):Subject_info(fileLoop).events.calibration.(event_types{eventos})(iii,2);
            scatter(event_plotted,1+zeros(1,length(event_plotted)), '.')
        end
    end
    legend(h(1:2))
    title(Subject_info(fileLoop).subjectID)
end


%% plot triggers
hold on
plot(Subject_info(fileLoop).OtherData.EventsTrigger, 'g')