%testSqueezeReleaseFile
clear all
close all
global one two subjectID leftOK rightOK state sq_thresh rel_thresh leftIndex rightIndex baseline1 baseline2  ;

one = false;
two = false;
leftOK = false;
rightOK = false;
sq_thresh = [0.104689 0.108482];
rel_thresh = [-0.105249 -0.103565];
subjectID = '';
leftIndex = 0;
rightIndex = 0;
baseline1 = 0;
baseline2 = 0;
state = 'noPress';

 fid1 = fopen(['data' '/data.csv'],'w');
 %fid2 = fopen('data/20191224_1357_PSAP_Eva_dddd/data.csv','r');
 fid2 = fopen('testm707.csv','r');
 %fid2 = fopen('test.csv','r');
    fprintf('opening connection...\n');
    if (exist('at')>0)
    clear('at')
    end
    at = createFileExperimentTimer(fid1,fid2);  
    start(at);
    % Get squeeze strengths
    % get all presses

%stateArr = {'nopress','strongR','strongL','lightR','lightL','strongR','strongL','lightR','lightL','strongR','strongL','lightR','lightL'};
stateArr = {'nopress','nopress','nopress','nopress','nopress','nopress','nopress','nopress','nopress','nopress','nopress','nopress'};
index = 1;
%q_thresh 0.194689 0.138482 rel_thresh -0.315249 -0.243565
%showImageNwait( CrossImage, window, windowRect, 4) %cross image
fprintf('cross image 4 seconds');
%waitForSecs(4)

%     fprintf('preparing for squeeze strengths...\n');
%      while index<=length(stateArr)
%         state = stateArr{index};
%         %one = false;
%         %two = false;
%         leftOK = false;
%         rightOK = false;
%         %showImageNwait( CrossImage, window, windowRect, 4) %cross image
%         fprintf('beginning squeezes + %s + %d\n',state,index);
%         fprintf('cross image 4 seconds\n');
%         waitForSecs(4)
%         fprintf('cross image 3 seconds\n');
%         waitForSecs(3)
%         fprintf('cross image 4 seconds\n');
%         waitForSecs(4)
%         %showImageNwait( beginImage, window, windowRect, 4) %begin image
%         %showImageNwait( CrossImage, window, windowRect, 4)
%         %showImageNwait( CrossImage, window, windowRect, 4) %cross image
%         index = index+1;
%         
%              
%         if (strcmp(state,'lightL')) 
%             if ~leftOK
%                 fprintf('press again left %s %d\n',state,index);
%                 %showImageNwait( PressAgainImage, window, windowRect, 4)
%                 index = index - 1;
%             else
%                 leftIndex = leftIndex+1;
%             end
%         end
%         if (strcmp(state,'lightR')) 
%             if ~rightOK
%                 fprintf('press again right %s %d\n',state,index);
%             %showImageNwait( PressAgainImage, window, windowRect, 4)
%             index = index - 1;
%             else
%                 rightIndex = rightIndex+1;
%             end
%         end
%         
%      end
%     
     %%
     FlushEvents('keydown')
     while(true)
      if (one && two)
          %sprintf('monetary')
          figure(1);plot(datetime('now','Format','HH:mm:ss.SSS'),2,'dk')
      elseif (one || two)
           %sprintf('revenge')
           figure(1);plot(datetime('now','Format','HH:mm:ss.SSS'),1,'dk')
      else
           figure(1);plot(datetime('now','Format','HH:mm:ss.SSS'),0.5,'dk')
      end
      if (CharAvail)
          break;
      end
      pause(0.2)
     %waitForSecs(500);
     end


    %%
    stop(at);
    %fclose(fid1);
    
    %%
    
    function waitForSecs(secs)
    start = GetSecs;
    while(GetSecs-start<secs)
        pause(0.1)
    end
    end
    