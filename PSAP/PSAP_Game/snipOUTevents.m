%create event related pressure vectors
% or currentPressChannelFiltered,events_aggression...

%% checks how many empties there are
% for fileLoop = 1:length(Subject_info)
%     empty_event_vec(fileLoop) = isempty(Subject_info(fileLoop).events);
% end
% lengthOfMonetaryEvents = NaN(100,sum(~empty_event_vec));
% for fileLoop = 1:length(Subject_info)
%     if ~isempty(Subject_info(fileLoop).events)
%         lengthOfMonetaryEvents(1:length(Subject_info(fileLoop).events.TaskEvents.events_monetary),fileLoop) = [Subject_info(fileLoop).events.TaskEvents.events_monetary(:,2)-...
%             Subject_info(fileLoop).events.TaskEvents.events_monetary(:,1)];
%     end
% end


%%
current_event_type = {'events_monetary', 'events_provocation','events_aggression'};
typeVar = {'double'};
durationOfEvent = [19*1000, 1000, 9*1000]; %monetary


for eventLoop = 1:length(current_event_type) %loop for each event
    for fileLoop = find([Subject_info(:).Inclusion]) %loop for all subjects
            %load timings of current event
            current_event = Subject_info(fileLoop).events.TaskEvents.(current_event_type{eventLoop});
            %accounting for nans in the timings of events:
            [a,b] = find(isnan(current_event));
            if ~isempty(a)
                current_event(a,b) = current_event(a,1)+durationOfEvent(eventLoop);
            end
            
            %preallocating space for data
            T_pressures = table('Size',[durationOfEvent(eventLoop), length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
            T_slopes = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
            T_var = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
            T_changes = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
            
            %snip out events
            for ii=1:size(current_event,1) %loop the incidances of the current type of event
                clear dataVec; clear pressure_change; clear pressure_slope; clear pressure_var; %clear everything
                
                dataVecL = Subject_info(fileLoop).PressureDATA.Lpress_normalized(current_event(ii,1):current_event(ii,2));
                [pressure_changeL, pressure_slopeL, pressure_varL]  = ischange(dataVecL, 'linear'); %  'Threshold' should be greater than 1
                
                dataVecR = Subject_info(fileLoop).PressureDATA.Rpress_normalized(current_event(ii,1):current_event(ii,2));
                [pressure_changeR, pressure_slopeR, pressure_varR]  = ischange(dataVecR, 'linear'); %  'Threshold' should be greater than 1
                
                if length(dataVecL)>=durationOfEvent(eventLoop)
                    PRESSURE_vecL(:,ii) = [dataVecL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(dataVecL),1)];
                    PRESSURE_slopeL(:,ii) = [pressure_slopeL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_slopeL),1)];
                    PRESSURE_varL(:,ii) = [pressure_varL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_varL),1)];
                    PRESSURE_changeL(:,ii) = [pressure_changeL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_changeL),1)];
                    
                    PRESSURE_vecR(:,ii) = [dataVecR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(dataVecR),1)];
                    PRESSURE_slopeR(:,ii) = [pressure_slopeR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_slopeR),1)];
                    PRESSURE_varR(:,ii) = [pressure_varR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_varR),1)];
                    PRESSURE_changeR(:,ii) = [pressure_changeR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_changeR),1)];
                elseif length(dataVecL)<durationOfEvent(eventLoop)
                    % place in arrays
                    PRESSURE_vecL(:,ii) = [dataVecL; nan(durationOfEvent(eventLoop)-length(dataVecL),1)];
                    PRESSURE_slopeL(:,ii) = [pressure_slopeL; nan(durationOfEvent(eventLoop)-length(pressure_slopeL),1)];
                    PRESSURE_varL(:,ii) = [pressure_varL; nan(durationOfEvent(eventLoop)-length(pressure_varL),1)];
                    PRESSURE_changeL(:,ii) = [pressure_changeL; nan(durationOfEvent(eventLoop)-length(pressure_changeL),1)];
                    
                    PRESSURE_vecR(:,ii) = [dataVecR; nan(durationOfEvent(eventLoop)-length(dataVecR),1)];
                    PRESSURE_slopeR(:,ii) = [pressure_slopeR; nan(durationOfEvent(eventLoop)-length(pressure_slopeR),1)];
                    PRESSURE_varR(:,ii) = [pressure_varR; nan(durationOfEvent(eventLoop)-length(pressure_varR),1)];
                    PRESSURE_changeR(:,ii) = [pressure_changeR; nan(durationOfEvent(eventLoop)-length(pressure_changeR),1)];
                end
            end
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).PressureR = PRESSURE_vecR;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).SlopeR = PRESSURE_slopeR;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).VarianceR = PRESSURE_varR;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).ChangeR = PRESSURE_changeR;
            
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).PressureL = PRESSURE_vecL;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).SlopeL = PRESSURE_slopeL;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).VarianceL = PRESSURE_varL;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).ChangeL = PRESSURE_changeL;
            %mean_pressures = nanmean(PRESSURE_vecR,2);
            %             figure; plot(PRESSURE_vecL,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); hold on
            %             plot(mean_pressures, 'Color', [211 122 140]/255,'LineWidth',3); title(Subject_info(fileLoop).subjectID)
            PRESSURE_vecL = [];PRESSURE_slopeL = [];PRESSURE_varL = [];PRESSURE_changeL = [];
            PRESSURE_vecR = [];PRESSURE_slopeR = [];PRESSURE_varR = [];PRESSURE_changeR = [];
            
            fprintf('%s: for subject %s completed',current_event_type{eventLoop},Subject_info(fileLoop).subjectID)
            
    end
    
end

%% plot it, why is this good for?
%   %events_afeter_provocation = ;
%   figure; plot(Subject_info(1).EventRelatedPressure.events_monetary.PressureR,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); hold on
%   plot(nanmean(Subject_info(1).EventRelatedPressure.events_monetary.PressureR,2), 'Color', [211 122 140]/255,'LineWidth',3); title(Subject_info(1).subjectID)
%   figure; hold on
%   for nn = 1:length(Subject_info(1).events.TaskEvents.events_provocation_buffered)
%       strat_pt = Subject_info(1).events.TaskEvents.monetaryNumberOfProvocation(nn,2)-1000;
%       end_pt = Subject_info(1).events.TaskEvents.monetaryNumberOfProvocation(nn,2)+1000;
%       monum = Subject_info(1).events.TaskEvents.monetaryNumberOfProvocation(nn,1);
%       plot(Subject_info(1).EventRelatedPressure.events_monetary.PressureR(strat_pt:end_pt,monum),'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); hold on
%   end
% %zero everything other than events? or plot seperately? zero the
%starts? how should I handle pressure drops below 2.3?
% for ii=1:length(current_event) %loop the incidances of the current type of event
%     
%     
%     if toPlotOrNotToPlot
%         disp('Plotting ...')
%         subplot(2,2,1)
%         plot(PRESSURE_vecL(:,ii));hold on
%         title('datavec')
%         
%         subplot(2,2,2)
%         plot(PRESSURE_vecL(:,ii).*PRESSURE_changeL(:,ii));hold on
%         title('datavec*pressure')
%         
%         subplot(2,2,3)
%         plot(PRESSURE_slopeL(:,ii));hold on
%         title('pressure_slope')
%         
%         subplot(2,2,4)
%         plot(PRESSURE_varL(:,ii));hold on
%         title('pressure_var')
%         sgtitle('Monetary Pressure across all trials')
%     end
%     
%     
%     
% end
%%
%the way to plot this mess
%how to overlay the provocations?
% figure
% stackedplot(Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).PressureL)
% figure
% stackedplot(PressureStruct(fileLoop).slopes)
% figure
% stackedplot(PressureStruct(fileLoop).variance)