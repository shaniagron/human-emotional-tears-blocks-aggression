for fileLoop = find([Subject_info(:).Inclusion])
    ransomized_plot_value(fileLoop) = round(rand*perc);
    Subject_info(fileLoop).decisions.toPlotOrNotToPlot = ransomized_plot_value(fileLoop);  

end
fprintf('~%2.0f percent of the subjects will be plotted\n',sum(ransomized_plot_value)/length(ransomized_plot_value)*100);