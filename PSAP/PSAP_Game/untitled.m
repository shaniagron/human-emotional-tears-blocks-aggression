%% init
AssertOpenGL;
    Screen('Preference', 'SkipSyncTests', 0);
    %state = 'init';
    
    %get main screen
    screens=Screen('Screens');
    screenNumber=max(screens);
    %screenNumber = 1;
    
    % Open a double-buffered fullscreen window:
    window=Screen('OpenWindow',screenNumber);
   [width, height]=Screen('WindowSize', window); 
      
    % get images
    %{
    goLeft = imread('goLeftWait.tif', 'tiff');
    goRight = imread('goRightWait.tif', 'tiff');
    instructions = imread('beforeStarInstructions.tif', 'tiff');
    squares = imread('basic.tif', 'tif');
    starLeft = imread('leftStar.tif', 'tif');
    starRight = imread('rightStar.tif', 'tif');
    clickReady = imread('clickWhenReady.tif', 'tif');
    %}
    
    % show instructions
    %Screen('PutImage', window, instructions);
    white = WhiteIndex(screenNumber);
    black = BlackIndex(screenNumber);
    gray = floor((white + black) / 2);
    Screen('TextSize',window,200);
    plusDuration = 4;%time plus is shown in seconds
    
    %white plus on gray background
    Screen('FillRect', window, gray);
    DrawFormattedText(window,'+','center','center',white);
    Screen('Flip', window);
    
    pause(plusDuration);
    
    %two rectangles
    Screen('FrameRect', window, gray,[width/4);
    
    %
    sca;
    