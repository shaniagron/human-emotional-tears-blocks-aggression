DirectoryEventsDataFolder = '/Users/eva/Box/PSAP_PTB/data_to_analyze/fixed_events_TRs/';
clear fileloop;
for fileLoop = 1:length(Subject_info)
    filenames_temp = dir(sprintf('%s/*%s.csv',DirectoryEventsDataFolder,lower(Subject_info(fileLoop).subjectID))); %find the file that matches the subject
    
    if ~isempty(filenames_temp)
        [~,locs] =  findpeaks(Subject_info(fileLoop).OtherData.EventsTrigger,'MinPeakHeight',1); % checks the trigger channel, first peak should be first no press
        locs_corrected  = locs>=Subject_info(fileLoop).PressureDATA.runs(1); 
        locs(~locs_corrected) = [];
        
        subj_events = readtable([filenames_temp.folder '/' filenames_temp.name]); %load the event log (MATLAB output)
        
        % ind events
        ind_nopress = string(subj_events{:,3})=='nopress'; %find indices according to the event log
        
        %taking only the last event
        while sum(ind_nopress)>1
            ind_nopress(find(ind_nopress, 1,'first')) = 0;
        end
        
        %alighns times to events
        new_StartTime = [subj_events{:,1}*1000+locs(1)-subj_events{ind_nopress,1}*1000]; %change times
        new_EndTime = [subj_events{:,5}*1000+locs(1)-subj_events{ind_nopress,1}*1000];
        %% special cases
         if fileLoop==42
             new_StartTime = new_StartTime-3476.308594;
             new_EndTime = new_EndTime-3476.308594;
         end 
         if fileLoop==14
             new_StartTime = new_StartTime-11875.96146;
             new_EndTime = new_EndTime-11875.96146;
         end
         %%    
        subj_events.new_StartTime = new_StartTime;
        subj_events.new_EndTime = new_EndTime;
        
        
        ind_lightR = string(subj_events{:,3})=='lightR';
        ind_lightL = string(subj_events{:,3})=='lightL';
        
        ind_strongR = string(subj_events{:,3})=='strongR';
        ind_strongL = string(subj_events{:,3})=='strongL';
        
        % checks that calibrations are only after last no_press indices
        ind_lightR = clear_mult_calibrations(ind_nopress,ind_lightR);
        ind_lightL = clear_mult_calibrations(ind_nopress,ind_lightL);
        ind_strongR = clear_mult_calibrations(ind_nopress,ind_strongR);
        ind_strongL = clear_mult_calibrations(ind_nopress,ind_strongL);

        
        events_strongR = [subj_events{ind_strongR,6}, subj_events{ind_strongR,7}];
        events_strongL = [subj_events{ind_strongL,6}, subj_events{ind_strongL,7}];
        events_lightR = [subj_events{ind_lightR,6}, subj_events{ind_lightR,7}];
        events_lightL = [subj_events{ind_lightL,6}, subj_events{ind_lightL,7}];
        
        %accounting for Nans
        if sum(sum(isnan(events_lightL),2))>=1 %filling up the data in case of NaN's.
            events_lightL(isnan(events_lightL)) = events_lightL(find(isnan(events_lightL))-3)+events_lightL(1,2)-events_lightL(1,1);
        end
        
        
        
        % all the data to the structure
        Subject_info(fileLoop).events.fullProtocol = subj_events;
        Subject_info(fileLoop).events.calibration.ind_nopress = ind_nopress ;
        Subject_info(fileLoop).events.calibration.ind_lightR = ind_lightR ;
        Subject_info(fileLoop).events.calibration.ind_lightL = ind_lightL ;
        Subject_info(fileLoop).events.calibration.ind_strongR = ind_strongR ;
        Subject_info(fileLoop).events.calibration.ind_strongL = ind_strongL ;
        
        Subject_info(fileLoop).events.calibration.events_strongR = events_strongR ;
        Subject_info(fileLoop).events.calibration.events_strongL = events_strongL ;
        Subject_info(fileLoop).events.calibration.events_lightR = events_lightR ;
        Subject_info(fileLoop).events.calibration.events_lightL = events_lightL ;
        
        disp(['Loading events data for subject ' Subject_info(fileLoop).subjectID])
        clear subj_events
        %disp(fileLoop)
    
    elseif isempty(filenames_temp)
        disp(['Failed to load events data for subject ' Subject_info(fileLoop).subjectID])
        fprintf('fileLoop = %d\n', fileLoop)
    else
        disp(['Something weird happened, please check subject ' Subject_info(fileLoop).subjectID])
    end
end