for fileLoop = find([Subject_info(:).Inclusion])
    figure
    plot(Subject_info(fileLoop).PressureDATA.movmean(Subject_info(fileLoop).events.calibration.events_strongR(1,1):Subject_info(fileLoop).events.calibration.events_strongR(1,2),1)...
        -Subject_info(fileLoop).PressureDATA.movmean(1))
    hold on
    plot(Subject_info(fileLoop).PressureDATA.movmean(Subject_info(fileLoop).events.calibration.events_strongR(1,1):Subject_info(fileLoop).events.calibration.events_strongR(1,2),2)...
        -Subject_info(fileLoop).PressureDATA.movmean(1))
    
    figure
    hold on
    plot(Subject_info(fileLoop).PressureDATA.movmean); hold on
    event_plotted = Subject_info(fileLoop).PressureDATA.movmean(Subject_info(fileLoop).events.calibration.events_strongR(1,1):Subject_info(fileLoop).events.calibration.events_strongR(1,2),1)...
        -Subject_info(fileLoop).PressureDATA.movmean(1);
    scatter(event_plotted,2+zeros(1,length(event_plotted)), '.')
    
end