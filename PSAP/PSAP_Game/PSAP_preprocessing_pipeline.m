%% create protocols
%where the wild data grow
cd ('/Users/eva/Box/PSAP_PTB/data_to_analyze/fixed_events_TRs')
%where should we put the protocols
protocolsOutputFolder = '/Users/eva/Desktop/PSAP_fMRI/protocols/Visual_events_model_noTRskip_moneyWeights';
files_in_folder = dir('*.csv');
null_agg = [];null_none=[];
for nn = 3:size(files_in_folder,1)
    file_name_temp = files_in_folder(nn).name;
    current_data = readtable(file_name_temp);
    subj = file_name_temp((end-8):(end-4));
    
    scans={'run1','run2', 'run3', 'run4','mprage'};
    
    event_types = {'monetary', 'revenge', 'provocation', 'none'}; %add provocation stopped and provocations continued
    TRs = {'1st_TR', '2nd_TR','3rd_TR', '4th_TR'};
    TR2skip = 0;
    % TRs = {'1st_TR'};
    for ii=1:(length(scans)-1) %number of scans- mprage
        for jj = 1:length(event_types)
            reference_TR = current_data{contains(current_data.('state'),TRs{ii}) & current_data.('round')==ii,1}; 
           
            events_temp = current_data{contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii,1};
            events_temp(:,2) = current_data{contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii,5}-events_temp(:,1);
            events_temp(:,1) = events_temp(:,1)-reference_TR-TR2skip;%-TR2skip(ii); %tr's to skip
            %events_temp(:,3) = ones(size(events_temp,1),1); %weights for now
            
            %%% adding the amount of money to the protocol
            events_temp(:,3) = current_data{contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii,2};
            %%%
            
            if isempty(events_temp)
                events_temp = [0 0 0];
                if event_types{jj}=="revenge"
                    null_agg = [null_agg; {sprintf('%s %d %s',subj,ii,event_types{jj})}]; %vector of the empty EV's in case I need it
                    
                elseif event_types{jj}=="none"
                    null_none = [null_none; {sprintf('%s %d %s',subj,ii,event_types{jj})}]; %vector of the empty EV's in case I need it
                end
            end
            if ii==1 & jj==1
                mkdir(sprintf('%s/%s',protocolsOutputFolder,upper(subj)))
            end
            if isequal('run11',scans{ii})
                fileID = fopen(sprintf('%s/%s/%s_%s.txt',protocolsOutputFolder,upper(subj),scans{ii},event_types{jj}),'w');
            else
                fileID = fopen(sprintf('%s/%s/%s_%s.txt',protocolsOutputFolder, upper(subj),scans{ii},event_types{jj}),'w');
            end 
            if fileID==-1
                error('Cannot open file for writing: %s', sprintf('%s/%s/%s_%s.txt',protocolsOutputFolder, upper(subj),scans{ii},event_types{jj}));
            end
            
            fprintf(fileID, '%0.2f %0.2f %.0f\r\n',events_temp');
            fclose(fileID);
            clear events_temp
        end
    end
end
%null_agg = unique(null_agg);

%FEAT output: folder per person
%%
%demeaning maount weights
cd(protocolsOutputFolder)
files_list = dir;
files_list = files_list(4:end);
for subjLoop = 1:length(files_list)
    subj = files_list(subjLoop).name;
    cd(subj)
    protocol_list = dir('*.txt');
    for EV_list = 1:length(protocol_list)
        temp_table = readtable(protocol_list(EV_list).name,'ReadVariableNames',false,'Delimiter', ' '); %read table
        demean_vec = temp_table{:,3}; %extract wehights vector
        demean_vec = demean_vec-mean(demean_vec); %demean
        temp_table{:,3} = demean_vec; %put back
        
        fileID = fopen(sprintf('%s/%s/%s',protocolsOutputFolder, upper(subj),protocol_list(EV_list).name),'w'); %open file
        if fileID==-1
            error('Cannot open file for writing: %s', sprintf('%s/%s/%s',protocolsOutputFolder, upper(subj),protocol_list(EV_list).name));
        end
        fprintf(fileID, '%0.2f %0.2f %0.8f\r\n',temp_table{:,:}'); %print
        fclose(fileID); %close
    end
    cd ..
end
%% create protocols for special messed up subjects
%where the wild data grow
cd ('/Users/eva/Box/PSAP_PTB/data_to_analyze/fixed_events_TRs')
%where should we put the protocols
protocolsOutputFolder = '/Users/eva/Desktop/PSAP_fMRI/protocols/Visual_events_model_noTRskip';
files_in_folder = dir('*.csv');
null_agg = [];null_none=[];
for nn = 1:size(files_in_folder,1)
    file_name_temp = files_in_folder(nn).name;
    current_data = readtable(file_name_temp);
    subj = file_name_temp((end-8):(end-4));
    
    %scans={'run1','run11','run2', 'run3', 'run4','mprage'};
    scans={'run11','run2', 'run3', 'run4','mprage'};
    event_types = {'monetary', 'revenge', 'provocation', 'none'}; %add provocation stopped and provocations continued
    TRs = {'11st_TR' '2nd_TR','3rd_TR', '4th_TR'};
    TR2skip = 0; flagfirst=0;
    % TRs = {'1st_TR'};
    for ii=1:(length(scans)-2) %number of scans- mprage
        for jj = 1:length(event_types)
            reference_TR = current_data{strcmp(current_data.('state'),TRs{ii}) & current_data.('round')==ii,1}; 
%             if numel(reference_TR)>1 & ~flagfirst==1
%                 reference_TR = reference_TR(1);
%                 flagfirst=1;
%                 fileID = fopen(sprintf('%s/%s/%s_%s.txt',protocolsOutputFolder, upper(subj),scans{ii},event_types{jj}),'w');
%             elseif numel(reference_TR)>1 & flagfirst==1
%                 reference_TR = reference_TR(end);
%                 fileID = fopen(sprintf('%s/%s/%s_%s.txt',protocolsOutputFolder,upper(subj),'run11',event_types{jj}),'w');
%             end
%             if isequal('run11',scans{ii})
%                 reference_TR = reference_TR(end);
%             elseif isequal('run1',scans{ii})
%                 reference_TR = reference_TR(1);
%             end
            events_temp = current_data{contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii,1};
            events_temp(:,2) = current_data{contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii,5}-events_temp(:,1);
            events_temp(:,1) = events_temp(:,1)-reference_TR-TR2skip;%-TR2skip(ii); %tr's to skip
            events_temp(:,3) = ones(size(events_temp,1),1); %weights for now
            if isempty(events_temp)
                events_temp = [0 0 0];
                if event_types{jj}=="revenge"
                    null_agg = [null_agg; {sprintf('%s %d %s',subj,ii,event_types{jj})}]; %vector of the empty EV's in case I need it
                    
                elseif event_types{jj}=="none"
                    null_none = [null_none; {sprintf('%s %d %s',subj,ii,event_types{jj})}]; %vector of the empty EV's in case I need it
                end
            end
            if ii==1 & jj==1
                mkdir(sprintf('%s/%s',protocolsOutputFolder,upper(subj)))
            end
            fileID = fopen(sprintf('%s/%s/%s_%s.txt',protocolsOutputFolder, upper(subj),scans{ii},event_types{jj}),'w');
            
            if fileID==-1
                error('Cannot open file for writing: %s', sprintf('/Users/eva/Desktop/%s/%s_%s.txt',upper(subj),scans{ii},event_types{jj}));
            end
            
            fprintf(fileID, '%0.2f %0.2f %.0f\r\n',events_temp');
            fclose(fileID);
            clear events_temp
        end
    end
end
%null_agg = unique(null_agg);

%FEAT output: folder per person


%% create protocols BV
cd ('/Users/eva/Box/PSAP_PTB/data_to_analyze')
current_data = readtable('20180827_1534_PSAP_Eva_m_zo094.csv');
subj = 'ZO094';

scans={'run1','run2', 'run3', 'run4','mprage'};
event_types = {'monetary', 'revenge', 'provocation'}; %add provocation stopped and provocations continued
TRs = {'1st_TR', '2nd_TR','3rd_TR', '4th_TR'};
TR2skip = 2*2;
%for %load current data's for each subject
% TRs = {'1st_TR'};
for ii=1:(length(scans)-1) %number of scans- mprage
    for jj = 1:length(event_types)
        %TR2skip = ? %load from an external file, written manually
        reference_TR = current_data{contains(current_data.('state'),TRs{ii}) & current_data.('round')==ii,1}; %should i remove 2 TRs?
                if numel(reference_TR)>1
                   reference_TR = reference_TR(end);
                end
%         if isequal('run11',scans{ii})
%             reference_TR = reference_TR(end);
%         elseif isequal('run1',scans{ii})
%             reference_TR = reference_TR(1);
%         end
        events_temp = current_data{contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii,1};
        events_temp(:,2) = current_data{contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii,5};
        events_temp = events_temp-reference_TR-TR2skip; %tr's to skip
        events_temp = events_temp*1000;
        %events_temp(:,3) = ones(size(events_temp,1),1); %weights for now
        if isempty(events_temp)
            events_temp = [0 0];
        end
        
        fileID = fopen(sprintf('/Users/eva/Desktop/PSAP_fMRI/BV_protocols/%s/%s_%s.txt',upper(subj),scans{ii},event_types{jj}),'w');
                %fileID = fopen(sprintf('/Users/eva/Desktop/PSAP_fMRI/protocols/%s/run11_%s.txt',upper(subj),event_types{jj}),'w');

        if fileID==-1
            error('Cannot open file for writing: %s', sprintf('/Users/eva/Desktop/%s/%s_%s.txt',upper(subj),scans{ii},event_types{jj}));
        end
        
        fprintf(fileID, '%0.2f %0.2f\r\n',events_temp');
        fclose(fileID);
        clear events_temp
    end
end

%% odor protocols
close all

load('/Users/eva/Box/PSAP_PTB/AA989_run1_odor.mat')
plot(C2B1)
hold on
plot(diff(C2B1))
plot(diff(C2B1)>0.1)

plot(diff(C2B1)<-0.1)

%1000 Hz
onTimes = find(diff(C2B1)>0.1); %time point of ON
offTimes = find(diff(C2B1)<-0.1);  %time point of OFF
onTimes = onTimes([1:2:length(onTimes)])

plot(onTimes,ones(length(onTimes),1),'o')
plot(offTimes,-1*ones(length(offTimes),1),'o')

%onset
odor_protocol(:,1) = onTimes/1000;
%duration
odor_protocol(:,2) = (offTimes-onTimes)/1000;
%weight
odor_protocol(:,3) = ones(length(odor_protocol),1);


fileID = fopen('/Volumes/evas_backup/PSAP_fMRI/protocols/AA989/run1_odor_2.5s.txt','w')
%fileID = fopen(sprintf('/Users/eva/Desktop/PSAP_fMRI/protocols/%s/%s_%s.txt',upper(subj),scans{ii},event_types{jj}),'w');

% if fileID==-1
%     error('Cannot open file for writing: %s', sprintf('/Users/eva/Desktop/%s/%s_%s.txt',upper(subj),scans{ii},event_types{jj}));
% end

fprintf(fileID, '%0.2f %0.2f %.0f\r\n',odor_protocol');
fclose(fileID);

%% analyse presses
%read from labchart or from computer? (computer more reliable before
%changing the connections in the setup, recordinga after 01/11 lab chart is reliable)


%%
%find first TR
[pks,locs] = findpeaks(C3B1);
plot(C3B1); hold on; scatter(locs,pks)
figure;plot(C7B1(2.55*10^5:4.9*10^5))
hold on
a = findpeaks(C7B1(2.55*10^5:4.9*10^5));
scatter(a,C7B1(a))


plot(C7B1)
hold on
plot(C8B1)
%% %% colors
greenC = [90 130 15]/255; %women control
darkGreenC = [37 54 6]/255; %women hex
redC = [180 63 69]/255; %men control
darkRedC = [105 37 40]/255; %men hex
blueC = [64 123 182]/255;
shift = 0.3;
greyC = [128 128 128]/255;

greenC_vec = [90,131,15;86,125,14;82,120,13;79,115,13;75,110,12;72,105,12;68,100,11;65,95,10;61,89,10;58,84,9;51,74,8;47,69,7;44,64,7;40,59,6;37,54,6;33,50,2]/255;
redC_vec = [180,62,70;175,60,68;170,58,66;165,57,64;160,55,62;155,53,60;150,52,58;145,50,56;140,48,54;135,47,52;130,45,50;125,43,48;120,42,46;115,40,44;110,38,42;105,37,40]/255;
%% all events in one table
protocolsOutputFolder = '/Users/eva/Desktop/PSAP_fMRI/protocols/Provocation_behavior';
tableofALL = readtable('/Users/eva/Box/PSAP_PTB/PSAP_GeneralData.xlsx');
subjectsNfirstConditionTable = readtable('/Users/eva/Downloads/subjectNconditions.csv');
cd ('/Users/eva/Box/PSAP_PTB/data_to_analyze/fixed_events_TRs')
files_in_folder = dir('*.csv');

event_types = {'monetary', 'revenge', 'provocation', 'none'};
%preallocating space for events in the table
sz = [size(files_in_folder,1),10];
varTypes = {'cell','cell','double','double','double','double','double','double','double','double'};
AllEvents = table('Size',sz,'VariableTypes',varTypes,'VariableNames',...
    {'subject','gender','Agg_HEX', 'Agg_Ctrl','Mon_HEX', 'Mon_Ctrl','Prov_HEX', 'Prov_Ctrl','None_HEX', 'None_Ctrl'});
for nn = 1:size(files_in_folder,1)
    file_name_temp = files_in_folder(nn).name;
    current_data = readtable(file_name_temp);
    subj = file_name_temp((end-8):(end-4));
    if nn==1
       subj = 'w474';
   end
    middlePt = find(contains(current_data.state,'AnatomicalScan')); %find location of anatomical scan, read all events before and after
    AggressionEventsTemp(1,1) = sum(contains(current_data{1:middlePt,3},'revenge'));
    AggressionEventsTemp(1,2) = sum(contains(current_data{middlePt:end,3},'revenge'));
    MonetaryEventsTemp(1,1) = sum(contains(current_data{1:middlePt,3},'monetary'));
    MonetaryEventsTemp(1,2) = sum(contains(current_data{middlePt:end,3},'monetary'));
    ProvocationEventsTemp(1,1) = sum(contains(current_data{1:middlePt,3},'provocation'));
    ProvocationEventsTemp(1,2) = sum(contains(current_data{middlePt:end,3},'provocation'));
    NoneEventsTemp(1,1) = sum(contains(current_data{1:middlePt,3},'none'));
    NoneEventsTemp(1,2) = sum(contains(current_data{middlePt:end,3},'none'));
    
%    %protovocls for behavior after provocation
%    postProvBehavior = current_data{find(contains(current_data{:,3},'provocation'))+2,3};
%    mkdir(sprintf('%s/%s',protocolsOutputFolder,upper(subj)))
%    
%    fileID = fopen(sprintf('%s/%s/%s_%s.txt',protocolsOutputFolder, upper(subj),scans{ii},event_types{jj}),'w');
%    %fileID = fopen(sprintf('/Users/eva/Desktop/PSAP_fMRI/protocols/%s/run11_%s.txt',upper(subj),event_types{jj}),'w');
%    
%    if fileID==-1
%        error('Cannot open file for writing: %s', sprintf('/Users/eva/Desktop/%s/%s_%s.txt',upper(subj),scans{ii},event_types{jj}));
%    end
%    
%    fprintf(fileID, '%0.2f %0.2f %.0f\r\n',events_temp');
%    fclose(fileID);
%    clear events_temp

    %inserting Events to the table
    AllEvents(nn,1) = {subj};
    AllEvents(nn,2) = tableofALL(contains(tableofALL.subject,subj,'IgnoreCase',true),3);
    ConditionTemp = subjectsNfirstConditionTable{contains(subjectsNfirstConditionTable.SUBJECT,subj,'IgnoreCase',true),2};
    if nn==1
      ConditionTemp = 'HEX';
    end
    if string(ConditionTemp)=='HEX'
        AllEvents(nn,3:end) = {AggressionEventsTemp(1,1),AggressionEventsTemp(1,2),MonetaryEventsTemp(1,1),MonetaryEventsTemp(1,2),...
            ProvocationEventsTemp(1,1),ProvocationEventsTemp(1,2),NoneEventsTemp(1,1), NoneEventsTemp(1,2)};
    elseif string(ConditionTemp)=='Control'
        AllEvents(nn,3:end) = {AggressionEventsTemp(1,2),AggressionEventsTemp(1,1),MonetaryEventsTemp(1,2),MonetaryEventsTemp(1,1),...
            ProvocationEventsTemp(1,2),ProvocationEventsTemp(1,1),NoneEventsTemp(1,2), NoneEventsTemp(1,1)};
    end 
    
end
%% All events
AllEvents = readtable('/Users/eva/Box/PSAP_PTB/PSAP_GeneralData.xlsx','Sheet', 'AllEventsTable');

histogram(AllEvents.(3)+AllEvents.(4),50)
title('hist of sum of subjects aggressive events')
AggressionMagicNumber = 5;
AggSelectionVec = AllEvents.(3)>AggressionMagicNumber & AllEvents.(4)>AggressionMagicNumber;%AllEvents.(3)+AllEvents.(4)>AggressionMagicNumber;
subjects_forAgg_analysis  = AllEvents(AggSelectionVec,1); %list of names for aggression analysis
fprintf('%d out of %d have more than %d aggressive responses in total\n',sum(AggSelectionVec), length(AggSelectionVec), AggressionMagicNumber)
disp('   * stats on men and women *   ')
tabulate(AllEvents.(2))
disp('   * men and women with enough aggression (included) *   ')
tabulate(AllEvents{AggSelectionVec,2})
disp('   * men and women without enough aggression (excluded) *   ')
tabulate(AllEvents{~AggSelectionVec,2})

%plot the change in subjects remaining in analysis with different AggressionMagicNumber
for ii = 1:12
    AggressionMagicNumber = ii;
    AggSelectionVec = AllEvents.(3)>AggressionMagicNumber & AllEvents.(4)>AggressionMagicNumber;%AllEvents.(3)+AllEvents.(4)>AggressionMagicNumber;
    sum_Selection(ii) = sum(AggSelectionVec);
end
figure, plot(sum_Selection)
%ylim([0 42])

%some numbers
%runs with 0 aggressive events


%% plots
figure; colormap('winter')
scatter3(AllEvents.(3),AllEvents.(5),AllEvents.(7),70,strcmp('m',AllEvents.(2)),'filled')
xlabel('Aggression');ylabel('Monetary');zlabel('Provocations')

figure;
colormap('spring')
scatter3(AllEvents.(4),AllEvents.(6),AllEvents.(8),70,strcmp('m',AllEvents.(2)),'filled')
xlabel('Aggression');ylabel('Monetary');zlabel('Provocations')

figure;colormap('spring')
scatter3(AllEvents.(3)-AllEvents.(4),AllEvents.(5)-AllEvents.(6),AllEvents.(7)-AllEvents.(8),70,strcmp('m',AllEvents.(2)),'filled')
xlabel('DeltaAggression');ylabel('DeltaMonetary');zlabel('DeltaProvocations')

%%
%scatter(agg HEX/prov HEX, agg Ctrl/prov Ctrl)
m_ind = string(AllEvents{:,2})=='m';
figure
scatter(AllEvents{m_ind,3}./AllEvents{m_ind,7},AllEvents{m_ind,4}./AllEvents{m_ind,8},100,'filled', 'MarkerFaceColor', redC,'MarkerFaceAlpha', 0.6)
hold on
scatter(AllEvents{~m_ind,3}./AllEvents{~m_ind,7},AllEvents{~m_ind,4}./AllEvents{~m_ind,8},100,'filled', 'MarkerFaceColor', greenC,'MarkerFaceAlpha', 0.6)
l = line([000 7], [000 7]);
l.LineWidth = 2;
l.Color = 'k';
l.LineStyle = '--';


%%
%% create protocols for provocations only
%this part creates a protocol where the first column indicates the times of
%a provocation and the second column says if the response afterwards was
%aggressive (1) or not (0)
%where the wild data grow
cd ('/Users/eva/Box/PSAP_PTB/data_to_analyze/fixed_events_TRs')
%where should we put the protocols
protocolsOutputFolder = '/Users/eva/Desktop/PSAP_fMRI/protocols/Provocation_behavior';
files_in_folder = dir('*.csv');
for nn = 2:size(files_in_folder,1)
    file_name_temp = files_in_folder(nn).name;
    current_data = readtable(file_name_temp);
    subj = file_name_temp((end-8):(end-4));
    
    scans={'run1','run2', 'run3', 'run4','mprage'};
    event_types = {'provocation'}; %add provocation stopped and provocations continued
    TRs = {'1st_TR', '2nd_TR','3rd_TR', '4th_TR'};
    TR2skip = 0;
    %TR2skip = [20*2 5*2 26*2 2*2];
    %for %load current data's for each subject
    % TRs = {'1st_TR'};
    for ii=1:(length(scans)-1) %number of scans- mprage
        for jj = 1:length(event_types)
            reference_TR = current_data{contains(current_data.('state'),TRs{ii}) & current_data.('round')==ii,1}; %should i remove 2 TRs? no
            events_temp = current_data{contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii,1};
            response_ind_temp = find(contains(current_data.('state'),event_types{jj}) & current_data.('round')==ii)+2;
            %%% in the case of a provocation at the end of a run
            if sum(response_ind_temp>size(current_data,1))>=1
                fprintf('check: subject %s, run %d, had a near end provocation, please check it\n',subj,jj)
            end
            
            Aggressive_events = current_data{contains(current_data.('state'),'revenge') & current_data.('round')==ii,3};
            Aggressive_events(response_ind_temp>size(current_data,1))={NaN};
            events_temp(response_ind_temp>size(current_data,1))=NaN;
            response_ind_temp(response_ind_temp>size(current_data,1))=NaN;
            ResponseAfterProv = current_data{response_ind_temp(~isnan(response_ind_temp)),3};
            
            events_temp(1:length(ResponseAfterProv),2) = contains(ResponseAfterProv,'revenge');
            events_temp(:,1) = events_temp(:,1)-reference_TR-TR2skip;%-TR2skip(ii); %tr's to skip
            if isempty(events_temp)
                events_temp = [NaN NaN];
            end
            if ii==1 & jj==1
                %mkdir(sprintf('%s/%s',protocolsOutputFolder,upper(subj)))
                fileID = fopen(sprintf('%s/%s.txt',protocolsOutputFolder, upper(subj)),'w');
                if fileID==-1
                    error('Cannot open file for writing: %s', sprintf('/Users/eva/Desktop/%s/%s_%s.txt',upper(subj),scans{ii},event_types{jj}));
                end
                
            end
            fileID = fopen(sprintf('%s/%s.txt',protocolsOutputFolder, upper(subj)),'a');
            
            fprintf(fileID, '%0.2f %0.0f\r\n',events_temp');
            
            clear events_temp
            clear ResponseAfterProv
        end
        fclose(fileID);
    end
    
    
end

%% create protocols for monetary - 6 secs, aggression - end of provocation
%
cd ('/Users/eva/Desktop/PSAP_fMRI/protocols/Visual_events_model_noTRskip')
%where should we put the protocols
protocolsOutputFolder = '/Users/eva/Desktop/PSAP_fMRI/protocolsVisual_events_model_6secMON_0SecProv_agg';
files_in_folder = dir;
null_agg = [];null_none=[];
%*********
%choose!
mon_event_length = 6;
duration_of_agg_event = 0;
AggDescision = 0; %2 secs before aggression onset
%*********

for nn = 4:size(files_in_folder,1)
    file_name_temp = files_in_folder(nn).name;
    cd(file_name_temp)
    
    for runnum=1:4
        %for monetary
        current_data = readtable(sprintf('run%d_monetary.txt',runnum),'ReadVariableNames',false,'Delimiter', ' ');
        
        subj = file_name_temp;
        event_types = current_data{:,1:3};
        
        event_types(event_types(:,2)>mon_event_length,2) = mon_event_length;
        
        if runnum==1
            mkdir(sprintf('%s/%s',protocolsOutputFolder,upper(subj)))
        end
        fileID = fopen(sprintf('%s/%s/run%d_monetary.txt',protocolsOutputFolder,file_name_temp,runnum),'w');
        
        
        if fileID==-1
            error('Cannot open file for writing: %s', sprintf('%s/%s/run%d_monetary.txt',protocolsOutputFolder,file_name_temp,runnum));
        end
        
        fprintf(fileID, '%0.2f %0.2f %.0f\r\n',event_types');
        fclose(fileID);
        clear event_types;clear current_data
        
        %for prov0 model
        current_data = readtable(sprintf('run%d_provocation.txt',runnum),'ReadVariableNames',false,'Delimiter', ' ');
        
        event_types(:,1) = current_data{:,1}; %moment of procvocation
        event_types(:,2) = duration_of_agg_event*ones(length(event_types),1);%length of model
        event_types(:,3) = ones(size(event_types,1),1);%wheights
        
        
        fileID = fopen(sprintf('%s/%s/run%d_EndOfProvocation.txt',protocolsOutputFolder,file_name_temp,runnum),'w');
        
        if fileID==-1
            error('Cannot open file for writing: %s', sprintf('%s/%s/run%d_monetary.txt',protocolsOutputFolder,file_name_temp,runnum));
        end
        
        fprintf(fileID, '%0.2f %0.2f %.0f\r\n',event_types');
        fclose(fileID);
        clear event_types; clear current_data
        
        %for PREaggression events
        current_data = readtable(sprintf('run%d_revenge.txt',runnum),'ReadVariableNames',false,'Delimiter', ' ');
        
        event_types = current_data{:,1:3};
        
        if sum(event_types)>0
            event_types(:,1) = event_types(:,1)-AggDescision;
            event_types(:,2) = ones(size(event_types,1),1);
        end
        fileID = fopen(sprintf('%s/%s/run%d_PREaggression.txt',protocolsOutputFolder,file_name_temp,runnum),'w');
        
        if fileID==-1
            error('Cannot open file for writing: %s', sprintf('%s/%s/run%d_PREaggression.txt',protocolsOutputFolder,file_name_temp,runnum));
        end
        
        fprintf(fileID, '%0.2f %0.2f %.0f\r\n',event_types');
        fclose(fileID);
        clear current_data; clear event_types
        
        %for none events
        current_data = readtable(sprintf('run%d_none.txt',runnum),'ReadVariableNames',false,'Delimiter', ' ');
        
        event_types = current_data{:,1:3};
        
        
        fileID = fopen(sprintf('%s/%s/run%d_none.txt',protocolsOutputFolder,file_name_temp,runnum),'w');
        
        if fileID==-1
            error('Cannot open file for writing: %s', sprintf('%s/%s/run%d_none.txt',protocolsOutputFolder,file_name_temp,runnum));
        end
        
        fprintf(fileID, '%0.2f %0.2f %.0f\r\n',event_types');
        fclose(fileID);
        clear current_data

        
    end
    cd .. %back to all subjects folder
end