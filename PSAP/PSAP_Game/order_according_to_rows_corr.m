function [mat_new, idx_new] = order_according_to_rows_corr(mat, th, metric)
%ORDER_ACCORDING_TO_ROWS_CORR Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3
    metric = 'correlation';
end
L = size(mat, 1);
mat_tree = linkage(mat,'average', metric);
mat_tree_repr = build_tree(mat_tree, size(mat_tree, 1), L);
if ~strcmp(metric, 'corrleation')
    th = mat_tree(floor(L * th), 3);
end

c = order_func(mat_tree_repr, th, {[]});
[~,I1] = sort(cellfun(@length,c), 'descend');
c = c(I1);

c2_new_concat = c{1}; %{I1(1)};
c2_cands = c;
c2_cands = c2_cands(2:end);
while length(c2_cands) > 1
    [~,I_dists] = sort(cellfun(@(x)mean(pdist2(mean(mat(c2_new_concat, :), 1), mat(x, :), 'correlation'), 2), c2_cands));
    c2_new_concat = [c2_new_concat, c2_cands{I_dists(1)}];
    c2_cands = c2_cands(I_dists(2:end));
end
c2_new_concat = [c2_new_concat, c2_cands{1}];
mat_new = mat(c2_new_concat, :);
idx_new = c2_new_concat;
end

