disp('normalizingPressesToCalibration,\n extrcting calibration events, normalizing and demeaning pressure')
for fileLoop = 1:length(Subject_info)
    if Subject_info(fileLoop).Inclusion
        if Subject_info(fileLoop).decisions.normalizeToCalibration
            fprintf('calibrating subject %s\n', Subject_info(fileLoop).subjectID)
            for ii=1:3
               %first line - light press L x 3 repeats
                temp_mean(1, ii) = mean(Subject_info(fileLoop).PressureDATA.Lpress(Subject_info(fileLoop).events.calibration.events_lightL(ii,1):Subject_info(fileLoop).events.calibration.events_lightL(ii,2)));
               %second line - strong press L x 3 repeats
                temp_mean(2, ii) = mean(Subject_info(fileLoop).PressureDATA.Lpress(Subject_info(fileLoop).events.calibration.events_strongL(ii,1):Subject_info(fileLoop).events.calibration.events_strongL(ii,2)));
               %third line - light press R x 3 repeats
                temp_mean(3, ii) = mean(Subject_info(fileLoop).PressureDATA.Rpress(Subject_info(fileLoop).events.calibration.events_lightR(ii,1):Subject_info(fileLoop).events.calibration.events_lightR(ii,2))); 
               %fourth line - strong press R x 3 repeats
                temp_mean(4, ii) = mean(Subject_info(fileLoop).PressureDATA.Rpress(Subject_info(fileLoop).events.calibration.events_strongR(ii,1):Subject_info(fileLoop).events.calibration.events_strongR(ii,2))); 
            
                if temp_mean(1, ii)>temp_mean(2, ii)
                    a = temp_mean(2, ii);
                    b = temp_mean(1, ii);
                    temp_mean(1, ii) = a;
                    temp_mean(2, ii) = b;
                    clear a; clear b;
                    fprintf('Subject %s has strange Left %d calibration values, and should be inspected manually!\n',Subject_info(fileLoop).subjectID, ii)
                end
                
                if temp_mean(3, ii)>temp_mean(4, ii)
                    a = temp_mean(4, ii);
                    b = temp_mean(3, ii);
                    temp_mean(3, ii) = a;
                    temp_mean(4, ii) = b;
                    clear a; clear b;
                    fprintf('Subject %s has strange Right %d calibration values, and should be inspected manually!\n',Subject_info(fileLoop).subjectID, ii)
                end
            end
            
            %% remove baseline = 2.3, and make min 50% of mean of weakest press
            calibrationMeans = mean(temp_mean,2);
            %disp('normalizing and demeaning pressure')
            %        calibrationMeans(1,:) = calibrationMeans(1,:)*PercentageofMin;%50%of min
            %        calibrationMeans(3,:) = calibrationMeans(3,:)*PercentageofMin;%50%of min
            %calibrationMeans = calibrationMeans-2.3;
            
            Lpress_normalized = (Subject_info(fileLoop).PressureDATA.Lpress-calibrationMeans(1))/(calibrationMeans(2)-calibrationMeans(1));
            Rpress_normalized = (Subject_info(fileLoop).PressureDATA.Rpress-calibrationMeans(3))/(calibrationMeans(4)-calibrationMeans(3));
            
            Lpress_normalized = Lpress_normalized-mode(Lpress_normalized);
            Rpress_normalized = Rpress_normalized-mode(Rpress_normalized);
            
            if Subject_info(fileLoop).decisions.toPlotOrNotToPlot
                figure; subplot(2,2,1)
                plot(Lpress_normalized);hold on; plot(Subject_info(fileLoop).PressureDATA.Lpress); title('Left hand');legend('normalized', 'original')
                subplot(2,2,2)
                plot(Rpress_normalized);hold on; plot(Subject_info(fileLoop).PressureDATA.Rpress); title('Right hand')
                subplot(2,2,3)
                histogram(Lpress_normalized,50); hold on;histogram(Subject_info(fileLoop).PressureDATA.Lpress,50)
             
                subplot(2,2,4)
                histogram(Rpress_normalized,50); hold on;histogram(Subject_info(fileLoop).PressureDATA.Rpress,50)
                sgtitle(Subject_info(fileLoop).subjectID)
            end
            Subject_info(fileLoop).PressureDATA.Rpress_normalized = Rpress_normalized;
            Subject_info(fileLoop).PressureDATA.Lpress_normalized = Lpress_normalized;
            clear temp_mean; clear Lpress_normalized; clear Rpress_normalized; clear calibrationMeans;
        else
           Subject_info(fileLoop).PressureDATA.Lpress_z =zscore(Subject_info(fileLoop).PressureDATA.Lpress);
           Subject_info(fileLoop).PressureDATA.Rpress_z =zscore(Subject_info(fileLoop).PressureDATA.Rpress);
           fprintf('z scoring subject %s\n', Subject_info(fileLoop).subjectID)
           
           %plot!
%            if Subject_info(fileLoop).decisions.toPlotOrNotToPlot
%                 figure; subplot(2,2,1)
%                 plot(Subject_info(fileLoop).PressureDATA.Lpress_z);hold on; plot(Subject_info(fileLoop).PressureDATA.Lpress); title('Left hand');legend('Standardized', 'Original')
%                 subplot(2,2,2)
%                 plot(Subject_info(fileLoop).PressureDATA.Rpress_z);hold on; plot(Subject_info(fileLoop).PressureDATA.Rpress); title('Right hand')
%                 subplot(2,2,3)
%                 histogram(Subject_info(fileLoop).PressureDATA.Rpress_z,50); hold on;histogram(Subject_info(fileLoop).PressureDATA.Lpress,50)
%              
%                 subplot(2,2,4)
%                 histogram(Subject_info(fileLoop).PressureDATA.Rpress_z,50); hold on;histogram(Subject_info(fileLoop).PressureDATA.Rpress,50)
%                 sgtitle(Subject_info(fileLoop).subjectID)
%             end
        end
    else
        disp(['There are no events for subject ' Subject_info(fileLoop).subjectID])
        
    end
end

%%
% %add a display of what its doing
% disp('demeaning pressure')
% for fileLoop = find([Subject_info(:).Inclusion])
%     Subject_info(fileLoop).PressureDATA.Rpress_normalized = Subject_info(fileLoop).PressureDATA.Rpress_normalized-mode(Subject_info(fileLoop).PressureDATA.Rpress_normalized);
%     Subject_info(fileLoop).PressureDATA.Lpress_normalized = Subject_info(fileLoop).PressureDATA.Lpress_normalized-mode(Subject_info(fileLoop).PressureDATA.Lpress_normalized);
%    
% end
% %add an output of new mode, should be centered around zero!!!
