%loadPSAPdata_spacialHandle
if CurrentFileName==string('zo094.mat')
    Subject_info(fileLoop).PressureDATA.rawLC = [C7B3 C8B3];
    Subject_info(fileLoop).OtherData.OdorStimuli = C2B3;
    Subject_info(fileLoop).OtherData.fMRITrigger = C3B3;
    Subject_info(fileLoop).OtherData.EventsTrigger = C5B3;
    Subject_info(fileLoop).OtherData.RespirationL = C4B3;
    Subject_info(fileLoop).OtherData.RespirationR = C6B3;
    clear C1B1; clear C2B1; clear C3B1; clear C4B1; clear C5B1; clear C6B1; clear C7B1; clear C8B1
    clear C1B2; clear C2B2; clear C3B2; clear C4B2; clear C5B2; clear C6B2; clear C7B2; clear C8B2
    clear C1B3; clear C2B3; clear C3B3; clear C4B3; clear C5B3; clear C6B3; clear C7B3; clear C8B3
    disp(['Loading LabChart generated files for subject ' Subject_info(fileLoop).subjectID])
    n=n+1;
    
elseif CurrentFileName==string('w474.mat')
    a = [C7B1;C7B2];
    a1 = [C8B1;C8B2];
    Subject_info(fileLoop).PressureDATA.rawLC = [a a1];
    Subject_info(fileLoop).OtherData.OdorStimuli = [C2B1;C2B2];
    Subject_info(fileLoop).OtherData.fMRITrigger = [C3B1:C3B2];
    Subject_info(fileLoop).OtherData.EventsTrigger = [C5B1;C5B2];
    Subject_info(fileLoop).OtherData.RespirationL = [C4B1;C4B2];
    Subject_info(fileLoop).OtherData.RespirationR = [C6B1;C6B2];
    clear C1B1; clear C2B1; clear C3B1; clear C4B1; clear C5B1; clear C6B1; clear C7B1; clear C8B1
    clear C1B2; clear C2B2; clear C3B2; clear C4B2; clear C5B2; clear C6B2; clear C7B2; clear C8B2
    disp(['Loading LabChart generated files for subject ' Subject_info(fileLoop).subjectID])
    clear a; clear a1;
    n=n+1;
    
elseif CurrentFileName==string('m_YM726.mat')
    Subject_info(fileLoop).PressureDATA.rawLC = [C7B1 C8B1];
    Subject_info(fileLoop).OtherData.OdorStimuli = C2B1;
    Subject_info(fileLoop).OtherData.fMRITrigger = C3B1;
    Subject_info(fileLoop).OtherData.EventsTrigger = C5B1;
    Subject_info(fileLoop).OtherData.RespirationL = C4B1;
    Subject_info(fileLoop).OtherData.RespirationR = C6B1;
    clear C1B1; clear C2B1; clear C3B1; clear C4B1; clear C5B1; clear C6B1; clear C7B1; clear C8B1
    clear C1B2; clear C2B2; clear C3B2; clear C4B2; clear C5B2; clear C6B2; clear C7B2; clear C8B2
    disp(['Loading LabChart generated files for subject ' Subject_info(fileLoop).subjectID])
    n=n+1;
    
elseif CurrentFileName==string('bn369.mat')
    Subject_info(fileLoop).PressureDATA.rawLC = [C7B2 C8B2];
    Subject_info(fileLoop).OtherData.OdorStimuli = C2B2;
    Subject_info(fileLoop).OtherData.fMRITrigger = C3B2;
    Subject_info(fileLoop).OtherData.EventsTrigger = C5B2;
    Subject_info(fileLoop).OtherData.RespirationL = C4B2;
    Subject_info(fileLoop).OtherData.RespirationR = C6B2;
    clear C1B1; clear C2B1; clear C3B1; clear C4B1; clear C5B1; clear C6B1; clear C7B1; clear C8B1
    clear C1B2; clear C2B2; clear C3B2; clear C4B2; clear C5B2; clear C6B2; clear C7B2; clear C8B2
    disp(['Loading LabChart generated files for subject ' Subject_info(fileLoop).subjectID])
    n=n+1;
end
    