function [ind_cal] = clear_mult_calibrations(ind_nopress,ind_cal)
%Clears any calibration events that come before the no_press_ind, hence,
%happened before the latest calibration.

loc_ind_cal = find(ind_cal);
ind_cal(loc_ind_cal(find(ind_nopress)>loc_ind_cal)) = 0;


end

