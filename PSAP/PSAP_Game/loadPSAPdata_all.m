%%
clear all
%%
CodePath = pwd; %'/Users/eva/Box/PSAP_PTB/PSAP_PTB_exp';
addpath('/Users/eva/Box/PSAP_PTB/PSAP_PTB_exp');
cd ('/Users/eva/Box/PSAP_PTB/Labchart_mat_files')
Subject_info.PressureDATA = struct('runs',[],'rawLC',[],'rawM',[],'filtered',[],'normalized',[],'Left',[],'Right',[]);
Subject_info.OtherData = struct('OdorStimuli',[],'fMRITrigger',[],'EventsTrigger',[],'RespirationL',[],'RespirationR',[]);
Subject_info = struct('subjectID',[],'FileNameM',[], 'FileNameLC',[]);
INDevents = struct('INDmonetary',[],'INDprovocation',[],'INDaggression',[]);

%% decisions decisions decisions
%first you have to choose.
%*%*%
normalizeToCalibration = 0 %1/0 should the signal be normalized according to initial calibration?
FilterSignal = 0; %1/0 should the signal be filtered? 0 - no, 1 - filter from Lior, 2 - simple moving average
toPlotOrNotToPlot = 1; %1/0 should the script plot the data?
FilesfromLabchartOrMATLAB = 'L'; % L for labchart, M for MATLAB
%*%*%
%check just moving average, numeric diff
%%
files_in_folder = dir('*.mat');
n=0;
%load all files into vars
for fileLoop=1:size(files_in_folder,1)
    CurrentFileName = files_in_folder(fileLoop).name;
    load(CurrentFileName)
    subj = CurrentFileName(1:(end-4));
    Subject_info(fileLoop).subjectID = subj;
    Subject_info(fileLoop).FileNameLC = files_in_folder(fileLoop).name;
    Subject_info(fileLoop).decisions.FilesfromLabchartOrMATLAB = FilesfromLabchartOrMATLAB;
    Subject_info(fileLoop).decisions.FilterSignal = FilterSignal;
    Subject_info(fileLoop).decisions.normalizeToCalibration = normalizeToCalibration;
    Subject_info(fileLoop).decisions.toPlotOrNotToPlot = toPlotOrNotToPlot;
    if CurrentFileName==string('zo094.mat') || CurrentFileName==string('w474.mat') || CurrentFileName==string('m_YM726.mat') || CurrentFileName==string('bn369.mat')
        loadPSAPdata_spacialHandle
    else
        Subject_info(fileLoop).PressureDATA.rawLC = [C7B1 C8B1];
        Subject_info(fileLoop).OtherData.OdorStimuli = C2B1;
        Subject_info(fileLoop).OtherData.fMRITrigger = C3B1;
        Subject_info(fileLoop).OtherData.EventsTrigger = C5B1;
        Subject_info(fileLoop).OtherData.RespirationL = C4B1;
        Subject_info(fileLoop).OtherData.RespirationR = C6B1;
        clear C1B1; clear C2B1; clear C3B1; clear C4B1; clear C5B1; clear C6B1; clear C7B1; clear C8B1
        disp(['Loading LabChart generated files for subject ' Subject_info(fileLoop).subjectID])
        n=n+1;
    end
end
fprintf('loaded %d LabChart files in total\n',n)
n=0; clear fileLoop;
%%
aa = dir('/Users/eva/Box/PSAP_PTB/data_to_analyze/*PSAP*/data.csv');
for fileLoop=1:size(files_in_folder,1)
    file_ind = find(contains({aa.folder},Subject_info(fileLoop).subjectID,'IgnoreCase',true));
    if ~isempty(file_ind)
        disp('Loading MATLAB generated files')
        disp(Subject_info(fileLoop).subjectID)
        Subject_info(fileLoop).PressureDATA.rawM = readtable([aa(file_ind).folder '/' aa(1).name]);
        Subject_info(fileLoop).FileNameM = aa(file_ind).name;
        n=n+1;
    end
end
fprintf('loaded %d MATLAB files in total\n',n)
n=0; clear fileLoop;
%%
% DirectoryDataFolder = ('/Users/eva/Box/PSAP_PTB/data_to_analyze');
%% runs
%fill runs! find the start and end of the runs
runs_table = table('Size',[length(Subject_info) 8],'VariableTypes',{'single','string','double','double','double','double','double','double'},...
    'VariableNames',{'fileLoop', 'subjectID',...
    'runs1','runs2','runs3','runs4','runs5','runs6'});
runs_table_old = readtable('/Users/eva/Box/PSAP_PTB/runs_ind_partial');
for fileLoop = 1:length(Subject_info)
    %for fileLoop = [20 21 22 25 32 40 52 54 56]
    runs_ind = find(strcmpi(Subject_info(fileLoop).subjectID, runs_table_old{:,2}));
    if isempty(runs_ind)
        figure('Position', [10 400 1500 500])
        plot(Subject_info(fileLoop).PressureDATA.rawLC(:,1))
        runs = ginput(6);
        close all
        runs(:,2) = [];
        Subject_info(fileLoop).PressureDATA.runs = runs; %runs contains the start and end of the calibration and the four runs
        runsVar(:,fileLoop) = runs;%save this to load later
        runs_table{fileLoop,1} =fileLoop;
        runs_table{fileLoop,2} = {Subject_info(fileLoop).subjectID};
        runs_table{fileLoop,3:8} = runs';
        fprintf('runs for subject %s completed (fileLoop = %d)\n',Subject_info(fileLoop).subjectID,fileLoop)
        clear runs;
    else
        runs = runs_table_old{runs_ind,3:8};
        Subject_info(fileLoop).PressureDATA.runs = runs;
        runs_table{fileLoop,1} =fileLoop;
        runs_table{fileLoop,2} = string(Subject_info(fileLoop).subjectID);
        runs_table(fileLoop,3:8) = runs_table_old(runs_ind,3:8);
        clear runs;
    end
end

% for fileLoop = 1:length(Subject_info)
% runs_table{fileLoop,1} =fileLoop;
%     runs_table{fileLoop,2} ={Subject_info(fileLoop).subjectID};
% end
%% save the output?

writetable(runs_table, '/Users/eva/Box/PSAP_PTB/runs_ind_partial')
%%

cd(CodePath)