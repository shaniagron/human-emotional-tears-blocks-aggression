%% maxRZ
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    monetaryNumberOfProvocation = Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1);
    monetaryNumberOfProvocation(monetaryNumberOfProvocation==0) = []; %remove zeros if there are
    justMonetary = [1:length(Subject_info(fileLoop).events.TaskEvents.events_monetary)]'; %create a vector
    justMonetary(monetaryNumberOfProvocation) = []; %remove the provocations from teh vector
 
    mean_monetary_maxRZ(n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ...
        (1,justMonetary));
    mean_monetaryWProv_maxRZ(n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ...
        (1,monetaryNumberOfProvocation));
    mean_monetaryAfterProv_maxRZ(n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxRZ...
        (:,monetaryNumberOfProvocation(1:end-1,1)+1));
    mean_events_provocation(n) = mean(Subject_info(fileLoop).EventRelatedPressure.events_provocation.maxRZ);
    mean_events_aggression(n) = mean(Subject_info(fileLoop).EventRelatedPressure.events_aggression.maxRZ);
    
    n=n+1;
    
end
colors = brewermap(6,'RdGy');
figure
scatter(1, nanmean(mean_monetary_maxRZ), 100*nanstd(mean_monetary_maxRZ), colors(1,:), 'filled')
hold on
scatter(1, nanmean(mean_monetaryWProv_maxRZ), 100*nanstd(mean_monetaryWProv_maxRZ),colors(2,:), 'filled')
scatter(1, nanmean(mean_monetaryAfterProv_maxRZ), 100*nanstd(mean_monetaryAfterProv_maxRZ), colors(3,:), 'filled')
scatter(1, nanmean(mean_events_provocation), 100*nanstd(mean_events_provocation), colors(4,:), 'filled')
scatter(1, nanmean(mean_events_aggression), 100*nanstd(mean_events_aggression), colors(5,:), 'filled')

legend('only monetary', 'monetary with provocation', 'monetary after provocation',' provocation', 'aggression')


deltamonWProv = mean_monetaryWProv_maxRZ-mean_monetary_maxRZ;
deltaafterProv = mean_monetaryAfterProv_maxRZ-mean_monetary_maxRZ;
deltaProv = mean_events_provocation-mean_monetary_maxRZ;
deltaagg = mean_events_aggression-mean_monetary_maxRZ;
%%
figure
scatter(repmat(1, [1,44]), mean_monetary_maxRZ)
hold on
scatter(repmat(2, [1,44]), mean_monetaryWProv_maxRZ)
scatter(repmat(3, [1,44]), mean_monetaryAfterProv_maxRZ)
scatter(repmat(4, [1,44]), mean_events_provocation)
scatter(repmat(5, [1,44]), mean_events_aggression)

xlim([0.5 5.5])
boxplot([mean_monetary_maxRZ', mean_monetaryWProv_maxRZ', mean_monetaryAfterProv_maxRZ', mean_events_provocation',mean_events_aggression' ],...
    'Labels',{'monetary','monwprov','monafterprov','prov', 'aggression'})

[p,tbl,stats] = anova1([mean_monetary_maxRZ', mean_monetaryWProv_maxRZ', mean_monetaryAfterProv_maxRZ', mean_events_provocation',mean_events_aggression' ])
[c,m,h] = multcompare(stats)

%%
figure
scatter(repmat(1, [1,44]), deltamonWProv)
hold on
scatter(repmat(2, [1,44]), deltaafterProv)
scatter(repmat(3, [1,44]), deltaProv)
scatter(repmat(4, [1,44]), deltaagg)
xlim([0.5 4.5])
boxplot([deltamonWProv', deltaafterProv', deltaProv', deltaagg'], 'Labels',{'deltamonWProv','deltaafterProv','deltaProv','deltaagg'})

[p,tbl,stats] = anova1([deltamonWProv', deltaafterProv', deltaProv', deltaagg'])
[c,m,h] = multcompare(stats)



%% maxLZ
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    monetaryNumberOfProvocation = Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1);
    monetaryNumberOfProvocation(monetaryNumberOfProvocation==0) = []; %remove zeros if there are
    justMonetary = [1:length(Subject_info(fileLoop).events.TaskEvents.events_monetary)]'; %create a vector
    justMonetary(monetaryNumberOfProvocation) = []; %remove the provocations from teh vector
 
    mean_monetary_maxLZ(n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxLZ...
        (1,justMonetary));
    mean_monetaryWProv_maxLZ(n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxLZ...
        (1,monetaryNumberOfProvocation));
    mean_monetaryAfterProv_maxLZ(n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.maxLZ...
        (:,monetaryNumberOfProvocation(1:end-1,1)+1));
    mean_events_provocation(n) = mean(Subject_info(fileLoop).EventRelatedPressure.events_provocation.maxLZ);
    mean_events_aggression(n) = mean(Subject_info(fileLoop).EventRelatedPressure.events_aggression.maxLZ);
    
    n=n+1;
    
end
colors = brewermap(6,'RdGy');
figure
scatter(1, nanmean(mean_monetary_maxLZ), 100*nanstd(mean_monetary_maxLZ), colors(1,:), 'filled')
hold on
scatter(1, nanmean(mean_monetaryWProv_maxLZ), 100*nanstd(mean_monetaryWProv_maxLZ),colors(2,:), 'filled')
scatter(1, nanmean(mean_monetaryAfterProv_maxLZ), 100*nanstd(mean_monetaryAfterProv_maxLZ), colors(3,:), 'filled')
scatter(1, nanmean(mean_events_provocation), 100*nanstd(mean_events_provocation), colors(4,:), 'filled')
scatter(1, nanmean(mean_events_aggression), 100*nanstd(mean_events_aggression), colors(5,:), 'filled')

legend('only monetary', 'monetary with provocation', 'monetary after provocation',' provocation', 'aggression')


deltamonWProv = mean_monetaryWProv_maxLZ-mean_monetary_maxLZ;
deltaafterProv = mean_monetaryAfterProv_maxLZ-mean_monetary_maxLZ;
deltaProv = mean_events_provocation-mean_monetary_maxLZ;
deltaagg = mean_events_aggression-mean_monetary_maxLZ;
%%
figure
scatter(repmat(1, [1,44]), mean_monetary_maxLZ)
hold on
scatter(repmat(2, [1,44]), mean_monetaryWProv_maxLZ)
scatter(repmat(3, [1,44]), mean_monetaryAfterProv_maxLZ)
scatter(repmat(4, [1,44]), mean_events_provocation)
scatter(repmat(5, [1,44]), mean_events_aggression)

xlim([0.5 5.5])
boxplot([mean_monetary_maxLZ', mean_monetaryWProv_maxLZ', mean_monetaryAfterProv_maxLZ', mean_events_provocation',mean_events_aggression' ],...
    'Labels',{'monetary','monwprov','monafterprov','prov', 'aggression'})

[p,tbl,stats] = anova1([mean_monetary_maxLZ', mean_monetaryWProv_maxLZ', mean_monetaryAfterProv_maxLZ', mean_events_provocation',mean_events_aggression' ])
[c,m,h] = multcompare(stats)

%%
figure
scatter(repmat(1, [1,44]), deltamonWProv)
hold on
scatter(repmat(2, [1,44]), deltaafterProv)
scatter(repmat(3, [1,44]), deltaProv)
scatter(repmat(4, [1,44]), deltaagg)
xlim([0.5 4.5])
boxplot([deltamonWProv', deltaafterProv', deltaProv', deltaagg'], 'Labels',{'deltamonWProv','deltaafterProv','deltaProv','deltaagg'})

[p,tbl,stats] = anova1([deltamonWProv', deltaafterProv', deltaProv', deltaagg'])
[c,m,h] = multcompare(stats)

%histogram(deltaagg, 30)