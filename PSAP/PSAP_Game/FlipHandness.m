function [LPress,RPress, Handness, LPressZ, RPressZ] = FlipHandness(Lpress, Rpress, handness, LpressZ, RpressZ)
%FlipHandness flips R and L for a specific subject.
% That's it!
LPress = Rpress;
RPress = Lpress;
Handness = [handness(2), handness(1)];
LPressZ = RpressZ;
RPressZ = LpressZ;

end

