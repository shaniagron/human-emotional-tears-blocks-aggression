%% All sorts of plots - what do they mean? no one knows

%% one figure and thats it
%monetary plot!
% for fileLoop = find([Subject_info(:).Inclusion])
%     disp(Subject_info(fileLoop).subjectID)
%     for ii = 1:size(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR,2)
%         plot(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(:,ii),'k')%-...
%         %Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(1,ii),'k')
%         hold on
%         plot(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL(:,ii),'b')
%         
%     end
% end
%

%% hapaam beemet
%all monetary
limitPressure = 4800;
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    allmon(:,n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR,2);
    n=n+1;
end
figure; subplot(2,1,1)
Z_allmon= zscore(abs(allmon));
plot(abs(allmon),'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); title('all monetary'); hold on
plot(nanmean(abs(allmon),2),'Color', [211 122 140]/255,'LineWidth',3);
%ylim([0 limitPressure])
xlim([0 15000])

subplot(2,1,2)
plot(Z_allmon,'Color', [[121 121 121]/255 0.3],'LineWidth',0.1); title('all monetary'); hold on
plot(nanmean(Z_allmon,2),'Color', [211 122 140]/255,'LineWidth',3);
xlim([0 15000])
%%
% monetary with provocatoin
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    monprov_events = Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1);
    for iii = 1:monprov_events+1
        try
    amp(:,n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(:,iii),2);
        catch
        end
    end
    n=n+1;
end
figure;
 subplot(2,1,1)
plot(abs(amp),'Color', [[121 121 121]/255 0.4],'LineWidth',0.1);title('monetary with provocation');hold on
plot(nanmean(abs(amp),2),'Color', [211 122 140]/255,'LineWidth',3);
%ylim([0 limitPressure])

for iii = 1:size(amp,2)
    amp(isnan(amp(:,iii)),iii) = nanmean(abs(amp(:,iii)));
end
Z_amp= zscore(abs(amp));
subplot(2,1,2)
plot(Z_amp,'Color', [[121 121 121]/255 0.8],'LineWidth',0.1); title('monetary with provocation'); hold on
plot(nanmean(Z_amp,2),'Color', [211 122 140]/255,'LineWidth',3);
%%
% monetary after provocatoin
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    monprov_events = Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1);
    for iii = monprov_events+2
        try
    ampONE(:,n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(:,iii),2);
        catch
        end
    end
    n=n+1;
end
figure; subplot(2,1,1)
plot(abs(ampONE),'Color', [[121 121 121]/255 0.4],'LineWidth',0.1);title('monetary after provocation'); hold on
plot(nanmean(abs(ampONE),2),'Color', [211 122 140]/255,'LineWidth',3);
%ylim([0 limitPressure])
%xlim([0 15000])

for iii = 1:size(ampONE,2)
    ampONE(isnan(ampONE(:,iii)),iii) = nanmean(abs(ampONE(:,iii)));
end
Z_ampONE= zscore(abs(ampONE));
subplot(2,1,2)
plot(Z_ampONE,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); title('monetary after provocation'); hold on
plot(nanmean(Z_ampONE,2),'Color', [211 122 140]/255,'LineWidth',3);
xlim([0 15000])

%%
% monetary with no provocatoin
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    trialVEC = 1:size(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR,2);
    prov_vec = Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1);
    prov_vec(prov_vec==0) = [];
    trialVEC(prov_vec)=[];
    %for iii = trialVEC+1
       try
            amnp(:,n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(:,trialVEC+1),2);
        catch
        end
    %end
    n=n+1;
end
figure;subplot(2,1,1)
plot(abs(amnp),'Color', [[121 121 121]/255 0.4],'LineWidth',0.1);title('monetary without provocation'); hold on
plot(nanmean(abs(amnp),2),'Color', [211 122 140]/255,'LineWidth',3);
%ylim([0 limitPressure])

for iii = 1:size(amnp,2)
    amnp(isnan(amnp(:,iii)),iii) = nanmean(abs(amnp(:,iii)));
end
 Z_amnp= zscore(abs(amnp)); 
subplot(2,1,2)
plot(Z_amnp,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); title('monetary without provocation'); hold on
plot(nanmean(Z_amnp,2),'Color', [211 122 140]/255,'LineWidth',3);



% mean_persubject_mon_prov = 
% mean_persubject_mon_NOprov = 
% 

%% check for 
%cross correlation of the mean of the signal and the signal mean. should be
%size of filter or haf of it.... soemthing like that 

%%figure of post provocation monetary
%% with seperation to provocations
figure
for fileLoop = find([Subject_info(:).Inclusion])
  
    for ii = 1:size(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR,2)
    plot(abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(:,ii)-Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(1,ii)),...
        'Color',[sum(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii+1) 0 0])
%         -Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(1,ii),'k')
        %should I substract first from 
          
%      plot(abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL(:,ii)-Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL(1,ii)),...
%         'Color',[1/255,0,sum(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii)])
     hold on

    end
    disp(Subject_info(fileLoop).subjectID)
    
end
%% plot around provocations
figure
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    
    for ii = 1:size(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR,2) %for all the monetray event of the subject
        if sum(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii)
            plot([1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR)]-Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation...
                (find(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii),2),...
                abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(:,ii)-Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(1,ii)),...
                'Color', [121 121 121]/255)
            hold on
          aroundprov(:,n) = abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(:,ii)-Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(1,ii));
%           plot([1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL)]-Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation...
%                 (find(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii),2),...
%                 abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL(:,ii)-Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL(1,ii)),...
%                 'Color', [211 122 140]/255)
         n=n+1;    
        end
        
    end
    disp(Subject_info(fileLoop).subjectID)
    
%     hold on; plot([1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR)]-Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation...
%                 (find(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii),2), nanmean(aroundprov,2),'Color', [211 122 140]/255,'LineWidth',3);

end
%% plot monetary
figure
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    mon(:,n) = nanmean(abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR),2);
    %for ii = 1:size(Subject_info(fileLoop).events.TaskEvents.events_monetary,1)
        hold on
        %plot(abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR(:,ii)), 'Color', [[121 121 121]/255 0.4],'LineWidth',0.1)
       plot(mean(abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR),2), 'Color', [[121 121 121]/255 0.4],'LineWidth',0.1)
    %end
     n=n+1;
end
hold on
plot(nanmean(abs(mon),2),'Color', [211 122 140]/255,'LineWidth',3);


for iii = 1:size(mon,2)
    mon(isnan(mon(:,iii)),iii) = nanmean(abs(mon(:,iii)));
end
Z_allmon= zscore(abs(mon));
plot(Z_allmon,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); title('monetary'); hold on
plot(nanmean(Z_allmon,2),'Color', [211 122 140]/255,'LineWidth',3);
xlim([0 9000])


%% plot aggression
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    try
    allagg(:,n) = nanmean(Subject_info(fileLoop).EventRelatedPressure.events_aggression.PressureR,2);
    n=n+1;
    catch
    end
end

figure;
subplot(2,1,1)
plot(abs(allagg),'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); title('all aggression'); hold on
plot(nanmean(abs(allagg),2),'Color', [211 122 140]/255,'LineWidth',3);
for iii = 1:size(allagg,2)
    allagg(isnan(allagg(:,iii)),iii) = nanmean(abs(allagg(:,iii)));
end
Z_allagg= zscore(abs(allagg));
Z_allagg(:,mean(Z_allagg)>0)=[];
subplot(2,1,2)
plot(Z_allagg,'Color', [[121 121 121]/255 0.3],'LineWidth',0.1); title('aggression'); hold on
plot(nanmean(Z_allagg,2),'Color', [211 122 140]/255,'LineWidth',3);
xlim([0 6000])


 %% pie charts!
% X = [1/3];
% X = [8/26];
% pie(X)
% figure
% X = [5/14];
% pie(X)
% X = [6/58];
% pie(X)
% 
% X =[2/20]
% pie(X)
% who_aggressed = []; open who_aggressed
% find(~who_aggressed)
%% change around provocation
figure
n=1;
for fileLoop = find([Subject_info(:).Inclusion])
    
    for ii = 1:size(Subject_info(fileLoop).EventRelatedPressure.events_monetary.ChangeR,2) %for all the monetray event of the subject
        if sum(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii)
            plot([1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.ChangeR)]-Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation...
                (find(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii),2),...
                abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.ChangeR(:,ii)-Subject_info(fileLoop).EventRelatedPressure.events_monetary.ChangeR(1,ii)),...
                'Color', [121 121 121]/255)
            hold on
          aroundprov(:,n) = abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.ChangeR(:,ii)-Subject_info(fileLoop).EventRelatedPressure.events_monetary.ChangeR(1,ii));
%           plot([1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL)]-Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation...
%                 (find(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii),2),...
%                 abs(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL(:,ii)-Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureL(1,ii)),...
%                 'Color', [211 122 140]/255)
         n=n+1;    
        end
        
    end
    %disp(Subject_info(fileLoop).subjectID)
    
%     hold on; plot([1:length(Subject_info(fileLoop).EventRelatedPressure.events_monetary.PressureR)]-Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation...
%                 (find(Subject_info(fileLoop).events.TaskEvents.monetaryNumberOfProvocation(:,1)==ii),2), nanmean(aroundprov,2),'Color', [211 122 140]/255,'LineWidth',3);

end