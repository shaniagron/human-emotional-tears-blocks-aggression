current_event_type = {'events_monetary', 'events_provocation','events_aggression', 'events_provocation_buffered'};
typeVar = {'double'};
durationOfEvent = [19*1000, 1000, 9*1000, 6*1000]; %monetary


for eventLoop = 1:length(current_event_type) %loop for each event
    for fileLoop = find([Subject_info(:).Inclusion]) %loop for all subjects
            %load timings of current event
            current_event = Subject_info(fileLoop).events.TaskEvents.(current_event_type{eventLoop});
            %accounting for nans in the timings of events:
            [a,b] = find(isnan(current_event));
            if ~isempty(a)
                current_event(a,b) = current_event(a,1)+durationOfEvent(eventLoop);
            end
            
            %preallocating space for data
            T_pressures = table('Size',[durationOfEvent(eventLoop), length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
            T_slopes = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
            T_var = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
            T_changes = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
            
            %snip out events
            for ii=1:size(current_event,1) %loop the incidances of the current type of event
                clear dataVec; clear pressure_change; clear pressure_slope; clear pressure_var; %clear everything
                if length(Subject_info(fileLoop).PressureDATA.Lpress)>=current_event(ii,2)
                    dataVecL = Subject_info(fileLoop).PressureDATA.Lpress(current_event(ii,1):current_event(ii,2));
                elseif length(Subject_info(fileLoop).PressureDATA.Lpress)<current_event(ii,2)
                    dataVecL = Subject_info(fileLoop).PressureDATA.Lpress(current_event(ii,1):end);
                    fprintf('event #%d, L, in %s for subject %s was cut by subject input\n',ii, current_event_type{eventLoop},Subject_info(fileLoop).subjectID)
                end
                
                [pressure_changeL, pressure_slopeL, pressure_varL]  = ischange(dataVecL, 'linear'); %  'Threshold' should be greater than 1
                
                if length(Subject_info(fileLoop).PressureDATA.Lpress)>=current_event(ii,2)
                    dataVecR = Subject_info(fileLoop).PressureDATA.Rpress(current_event(ii,1):current_event(ii,2));
                elseif length(Subject_info(fileLoop).PressureDATA.Lpress)<current_event(ii,2)
                    dataVecR = Subject_info(fileLoop).PressureDATA.Rpress(current_event(ii,1):end);
                    fprintf('event #%d, R, in %s for subject %s was cut by subject input\n',ii, current_event_type{eventLoop},Subject_info(fileLoop).subjectID)
                end
                
                [pressure_changeR, pressure_slopeR, pressure_varR]  = ischange(dataVecR, 'linear'); %  'Threshold' should be greater than 1
                
                if length(dataVecL)>=durationOfEvent(eventLoop)
                    PRESSURE_vecL(:,ii) = [dataVecL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(dataVecL),1)];
                    PRESSURE_slopeL(:,ii) = [pressure_slopeL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_slopeL),1)];
                    PRESSURE_varL(:,ii) = [pressure_varL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_varL),1)];
                    PRESSURE_changeL(:,ii) = [pressure_changeL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_changeL),1)];
                    
                    PRESSURE_vecR(:,ii) = [dataVecR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(dataVecR),1)];
                    PRESSURE_slopeR(:,ii) = [pressure_slopeR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_slopeR),1)];
                    PRESSURE_varR(:,ii) = [pressure_varR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_varR),1)];
                    PRESSURE_changeR(:,ii) = [pressure_changeR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_changeR),1)];
                elseif length(dataVecL)<durationOfEvent(eventLoop)
                    % place in arrays
                    PRESSURE_vecL(:,ii) = [dataVecL; nan(durationOfEvent(eventLoop)-length(dataVecL),1)];
                    PRESSURE_slopeL(:,ii) = [pressure_slopeL; nan(durationOfEvent(eventLoop)-length(pressure_slopeL),1)];
                    PRESSURE_varL(:,ii) = [pressure_varL; nan(durationOfEvent(eventLoop)-length(pressure_varL),1)];
                    PRESSURE_changeL(:,ii) = [pressure_changeL; nan(durationOfEvent(eventLoop)-length(pressure_changeL),1)];
                    
                    PRESSURE_vecR(:,ii) = [dataVecR; nan(durationOfEvent(eventLoop)-length(dataVecR),1)];
                    PRESSURE_slopeR(:,ii) = [pressure_slopeR; nan(durationOfEvent(eventLoop)-length(pressure_slopeR),1)];
                    PRESSURE_varR(:,ii) = [pressure_varR; nan(durationOfEvent(eventLoop)-length(pressure_varR),1)];
                    PRESSURE_changeR(:,ii) = [pressure_changeR; nan(durationOfEvent(eventLoop)-length(pressure_changeR),1)];
                end
            end
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).PressureR = PRESSURE_vecR;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).SlopeR = PRESSURE_slopeR;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).VarianceR = PRESSURE_varR;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).ChangeR = PRESSURE_changeR;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).maxR = max(PRESSURE_vecR);
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).medianR = median(PRESSURE_vecR);

            
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).PressureL = PRESSURE_vecL;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).SlopeL = PRESSURE_slopeL;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).VarianceL = PRESSURE_varL;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).ChangeL = PRESSURE_changeL;
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).maxL = max(PRESSURE_vecL);
            Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).medianL = median(PRESSURE_vecL);

            %mean_pressures = nanmean(PRESSURE_vecR,2);
            %             figure; plot(PRESSURE_vecL,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); hold on
            %             plot(mean_pressures, 'Color', [211 122 140]/255,'LineWidth',3); title(Subject_info(fileLoop).subjectID)
            PRESSURE_vecL = [];PRESSURE_slopeL = [];PRESSURE_varL = [];PRESSURE_changeL = [];
            PRESSURE_vecR = [];PRESSURE_slopeR = [];PRESSURE_varR = [];PRESSURE_changeR = [];
            fprintf('%s: for subject %s completed\n',current_event_type{eventLoop},Subject_info(fileLoop).subjectID)
    end
    
end