disp(' *   snipOUTevents_Zscored    *')

current_event_type = {'events_monetary', 'events_provocation','events_aggression', 'events_provocation_buffered'};
typeVar = {'double'};
durationOfEvent = [19*1000, 1000, 9*1000, 6*1000]; %monetary


for eventLoop = 1:length(current_event_type) %loop for each event
    for fileLoop = find([Subject_info(:).Inclusion]) %loop for all subjects
        %load timings of current event
        current_event = Subject_info(fileLoop).events.TaskEvents.(current_event_type{eventLoop});
        %accounting for nans in the timings of events:
        [a,b] = find(isnan(current_event));
        if ~isempty(a)
            current_event(a,b) = current_event(a,1)+durationOfEvent(eventLoop);
        end
        
        %preallocating space for data
        T_pressures = table('Size',[durationOfEvent(eventLoop), length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
        T_slopes = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
        T_var = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
        T_changes = table('Size',[durationOfEvent(eventLoop),length(current_event)],'VariableTypes',repmat(typeVar,[1 length(current_event)]));
        aucZ_R = []; aucZ_L = []; aucZ_L_beforeOnset = []; aucZ_R_beforeOnset = [];
        
        %snip out events
        for ii=1:size(current_event,1) %loop the incidances of the current type of event
            clear dataVec; clear pressure_change; clear pressure_slope; clear pressure_var; %clear everything
            if length(Subject_info(fileLoop).PressureDATA.Lpress_z)>=current_event(ii,2)
                dataVecL = Subject_info(fileLoop).PressureDATA.Lpress_z(current_event(ii,1):current_event(ii,2));
            elseif length(Subject_info(fileLoop).PressureDATA.Lpress_z)<current_event(ii,2)
                dataVecL = Subject_info(fileLoop).PressureDATA.Lpress_z(current_event(ii,1):end);
                fprintf('event #%d, L, in %s for subject %s was cut by subject input\n',ii, current_event_type{eventLoop},Subject_info(fileLoop).subjectID)
            end
            
%             for iii = 1:3
%                 threshold_z(iii) = max( Subject_info(fileLoop).PressureDATA.Rpress_z(...
%                     Subject_info(fileLoop).events.calibration.events_lightR(iii,1): Subject_info(fileLoop).events.calibration.events_lightR(iii,2)));
%                 plot(Subject_info(fileLoop).PressureDATA.Rpress_z(...
%                     Subject_info(fileLoop).events.calibration.events_lightR(iii,1): Subject_info(fileLoop).events.calibration.events_lightR(iii,2)))
%                 hold on
%             end
            [pressure_changeL, pressure_slopeL, pressure_varL]  = ischange(dataVecL, 'linear'); %  'Threshold' should be greater than 1
           
            
            if length(Subject_info(fileLoop).PressureDATA.Lpress_z)>=current_event(ii,2)
                dataVecR = Subject_info(fileLoop).PressureDATA.Rpress_z(current_event(ii,1):current_event(ii,2));
            elseif length(Subject_info(fileLoop).PressureDATA.Lpress_z)<current_event(ii,2)
                dataVecR = Subject_info(fileLoop).PressureDATA.Rpress_z(current_event(ii,1):end);
                fprintf('event #%d, R, in %s for subject %s was cut by subject input\n',ii, current_event_type{eventLoop},Subject_info(fileLoop).subjectID)
            end
            
            [pressure_changeR, pressure_slopeR, pressure_varR]  = ischange(dataVecR, 'linear'); %  'Threshold' should be greater than 1
            if eventLoop==1 || eventLoop==3 %monetary or aggression
                aucZ_R(ii) = trapz(dataVecR);
                aucZ_L(ii) = trapz(dataVecL);
                try
                    aucZ_L_beforeOnset(ii) = trapz(dataVecL(1:secs_buffer));
                    aucZ_R_beforeOnset(ii) = trapz(dataVecR(1:secs_buffer));
                catch
                    fprintf('event #%d, %s ended early for subject %s\n',ii, current_event_type{eventLoop},Subject_info(fileLoop).subjectID)
                    aucZ_L_beforeOnset(ii) = nan;
                    aucZ_R_beforeOnset(ii) = nan;
                end
                %                     if length(dataVecL %if the event ended
                %                         aucZ_L_AfterEnd = trapz(dataVecL((end-3*secs_buffer):end));
                %                         aucZ_R_AfterEnd = trapz(dataVecR((end-3*secs_buffer):end));
                %                     end
            end
            
            if length(dataVecL)>=durationOfEvent(eventLoop)
                PRESSURE_vecL(:,ii) = [dataVecL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(dataVecL),1)];
                PRESSURE_slopeL(:,ii) = [pressure_slopeL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_slopeL),1)];
                PRESSURE_varL(:,ii) = [pressure_varL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_varL),1)];
                PRESSURE_changeL(:,ii) = [pressure_changeL(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_changeL),1)];
                
                PRESSURE_vecR(:,ii) = [dataVecR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(dataVecR),1)];
                PRESSURE_slopeR(:,ii) = [pressure_slopeR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_slopeR),1)];
                PRESSURE_varR(:,ii) = [pressure_varR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_varR),1)];
                PRESSURE_changeR(:,ii) = [pressure_changeR(1:durationOfEvent(eventLoop)); nan(durationOfEvent(eventLoop)-length(pressure_changeR),1)];
            elseif length(dataVecL)<durationOfEvent(eventLoop)
                % place in arrays
                PRESSURE_vecL(:,ii) = [dataVecL; nan(durationOfEvent(eventLoop)-length(dataVecL),1)];
                PRESSURE_slopeL(:,ii) = [pressure_slopeL; nan(durationOfEvent(eventLoop)-length(pressure_slopeL),1)];
                PRESSURE_varL(:,ii) = [pressure_varL; nan(durationOfEvent(eventLoop)-length(pressure_varL),1)];
                PRESSURE_changeL(:,ii) = [pressure_changeL; nan(durationOfEvent(eventLoop)-length(pressure_changeL),1)];
                
                PRESSURE_vecR(:,ii) = [dataVecR; nan(durationOfEvent(eventLoop)-length(dataVecR),1)];
                PRESSURE_slopeR(:,ii) = [pressure_slopeR; nan(durationOfEvent(eventLoop)-length(pressure_slopeR),1)];
                PRESSURE_varR(:,ii) = [pressure_varR; nan(durationOfEvent(eventLoop)-length(pressure_varR),1)];
                PRESSURE_changeR(:,ii) = [pressure_changeR; nan(durationOfEvent(eventLoop)-length(pressure_changeR),1)];
            end
            limit_detection = min([4000,length(pressure_slopeL)]); %the shorter of both
            
            [~,I] = max(pressure_slopeL(1:limit_detection)); %add a limit to where this max slope is
            [~,II] = max(pressure_slopeR(1:limit_detection));
            latency_L(ii) = 2000 - I;
            latency_R(ii) = 2000 - II;

        end
        %all parameters excrated from a press
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).PressureRZ = PRESSURE_vecR;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).SlopeRZ = PRESSURE_slopeR;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).VarianceRZ = PRESSURE_varR;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).ChangeRZ = PRESSURE_changeR;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).maxRZ = max(PRESSURE_vecR);
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).medianRZ = nanmedian(PRESSURE_vecR);
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).AUCZ_R = aucZ_R;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).AUCZ_R_beforeOnset = aucZ_R_beforeOnset;
        %Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).AUCZ_R_AfterEnd = aucZ_R_AfterEnd;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).latency_R = latency_R;
        
        
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).PressureLZ = PRESSURE_vecL;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).SlopeLZ = PRESSURE_slopeL;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).VarianceLZ = PRESSURE_varL;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).ChangeLZ = PRESSURE_changeL;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).maxLZ = max(PRESSURE_vecL);
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).medianLZ = nanmedian(PRESSURE_vecL);
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).AUCZ_L = aucZ_L;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).AUCZ_L_beforeOnset = aucZ_L_beforeOnset;
        %Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).AUCZ_L_AfterEnd = aucZ_L_AfterEnd;
        Subject_info(fileLoop).EventRelatedPressure.(current_event_type{eventLoop}).latency_L = latency_L;
        
        %mean_pressures = nanmean(PRESSURE_vecR,2);
        %             figure; plot(PRESSURE_vecL,'Color', [[121 121 121]/255 0.4],'LineWidth',0.1); hold on
        %             plot(mean_pressures, 'Color', [211 122 140]/255,'LineWidth',3); title(Subject_info(fileLoop).subjectID)
        PRESSURE_vecL = []; PRESSURE_slopeL = []; PRESSURE_varL = []; PRESSURE_changeL = [];
        PRESSURE_vecR = []; PRESSURE_slopeR = []; PRESSURE_varR = []; PRESSURE_changeR = [];
        aucZ_L = []; aucZ_R = []; aucZ_L_AfterEnd = []; aucZ_R_AfterEnd = []; 
        aucZ_L_beforeOnset = []; aucZ_R_beforeOnset = [];
        
        fprintf('%s: for subject %s completed\n',current_event_type{eventLoop},Subject_info(fileLoop).subjectID)
    end
    
end

clear PRESSURE_vecL; clear PRESSURE_slopeL;clear PRESSURE_varL; clear PRESSURE_changeL;
clear PRESSURE_vecR; clear PRESSURE_slopeR; clear PRESSURE_varR; clear PRESSURE_changeR;
clear aucZ_L; clear aucZ_R; clear aucZ_L_AfterEnd; clear aucZ_R_AfterEnd;
clear aucZ_L_beforeOnset; clear aucZ_R_beforeOnset;
