for fileLoop = 1:length(Subject_info)
    if ~isempty(Subject_info(fileLoop).events)
        if  mean(Subject_info(fileLoop).PressureDATA.movmean(Subject_info(fileLoop).events.calibration.events_strongR(1,1):Subject_info(fileLoop).events.calibration.events_strongR(1,2),1)...
                -Subject_info(fileLoop).PressureDATA.movmean(1))...
                >mean(Subject_info(fileLoop).PressureDATA.movmean(Subject_info(fileLoop).events.calibration.events_strongR(1,1):Subject_info(fileLoop).events.calibration.events_strongR(1,2),2)...
                -Subject_info(fileLoop).PressureDATA.movmean(1))
            Rpress = Subject_info(fileLoop).PressureDATA.movmean(:,1);
            Lpress = Subject_info(fileLoop).PressureDATA.movmean(:,2);
            
            whichpress = [1 2];
        elseif mean(Subject_info(fileLoop).PressureDATA.movmean(Subject_info(fileLoop).events.calibration.events_strongR(1,1):Subject_info(fileLoop).events.calibration.events_strongR(1,2),1)...
                -Subject_info(fileLoop).PressureDATA.movmean(1))...
                <mean(Subject_info(fileLoop).PressureDATA.movmean(Subject_info(fileLoop).events.calibration.events_strongR(1,1):Subject_info(fileLoop).events.calibration.events_strongR(1,2),2)...
                -Subject_info(fileLoop).PressureDATA.movmean(1))
            Rpress = Subject_info(fileLoop).PressureDATA.movmean(:,2);
            Lpress = Subject_info(fileLoop).PressureDATA.movmean(:,1);
            
            whichpress = [2 1];
        else
            disp(['*** There is an error in determining L and R, please review subject ' Subject_info(fileLoop).subjectID ' ***'])
        end
        
        Subject_info(fileLoop).PressureDATA.Lpress =  Lpress;
        Subject_info(fileLoop).PressureDATA.Rpress =  Rpress;
        Subject_info(fileLoop).PressureDATA.Handness = whichpress;
        
        
        clear Lpress; clear Rpress; clear whichpress;
    else
        disp(['There are no events for subject ' Subject_info(fileLoop).subjectID])
        
    end
end

%open questions: how does this relate to the matlab pressure? and 