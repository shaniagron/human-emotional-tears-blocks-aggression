function groups = order_func(t, th, groups)
for branch = {'right', 'left'}
    child = t.(branch{1});
    if ~isstruct(child)
        groups{end}(end+1) = child;
    else
        groups = order_func(child, th, groups);
    end
    if t.distance > th && strcmp(branch{1}, 'right')
        groups{end+1} = [];
    end
end

end