global one two subjectID leftOK rightOK state;
one = false;
two = false;
leftOK = false;
rightOK = false;
subjectID = '';
PsychDebugWindowConfiguration


%% User Initialization


% insert subject number
prompt = {'Enter subject number:'}; %description of fields

answer = inputsdlg(prompt, 'Subject Number'); %opens dialog
subject = answer{1,:}; %Gets Subject Name
%
%sca


%% Psychtoolbox initializtion
fprintf('initializing psychtoolbox...\n');
Screen('Preference', 'SkipSyncTests', 1);  %IMPORTANT: for debug only! needs to be removed
Screen('Preference', 'ConserveVRAM', 64); %IMPORTANT: for debug only! needs to be removed
% Here we call some default settings for setting up Psychtoolbox
PsychDefaultSetup(1);

% Get the screen numbers
screens = Screen('Screens');

% Draw to the external screen if avaliable
screenNumber = max(screens);
white = WhiteIndex(screenNumber);
black = BlackIndex(screenNumber);
gray = floor((white + black) / 2);

[window, windowRect] = PsychImaging('OpenWindow', screenNumber, gray);
%InitializePsychSound;
% Get the size of the on screen window
[screenXpixels, screenYpixels] = Screen('WindowSize', window);


RedColor = [255 0 0];

 %% General Initalization
formatOut = 'ddmmyy';
date = datestr(now,formatOut);
% 
HomeDir = pwd;
baseDir = './data/'; 
subj = sprintf('%s_%s',subject,date);
mkdir(baseDir, subj);
dataDirectory = sprintf('data/%s',subj);
% imagesDirectory = 'stimuli/';
% ekmanDirectory = 'stimuli/ekman_VAS';
% finalQDirectory = 'stimuli/finalQ';
% NBGDirectory = 'stimuli/shapes';
% pleasantnessDirectory = 'stimuli/scales';
% ultimatumDirectory =  'stimuli/ultimatum inst';
% 
% rmpath(ekmanDirectory); rmpath(finalQDirectory); rmpath(NBGDirectory); 
% rmpath(pleasantnessDirectory); rmpath(ultimatumDirectory);
% 
% path(path, imagesDirectory);
% 
%PleaseWaitImage = 'wait.tiff';
CrossImage = 'cross.jpg';
beginImage = 'begin.tiff';
endImage = 'end.tiff';
PressAgainImage = 'no_press_input.tiff';
% 
% 
% minX = (253/1440)*(windowRect(3)-windowRect(1));
% maxX = (1188/1440)*(windowRect(3)-windowRect(1));
% minY = (472/900)*(windowRect(4)-windowRect(2));
% maxY = (522/900)*(windowRect(4)-windowRect(2));
xPos = windowRect(3)*.5; yPos = windowRect(4)*.73;
fprintf('loading functions...\n');
Screen('DrawText', window, 'initializing', xPos,yPos);%, [1 0 0]);
%% open hardware session
if (hardware)
    fid1 = fopen([dataDirectory '\data.csv'],'w');
    fprintf('opening connection...\n');
    clear t;
    t = createInputSession('both');
    lh = addlistener(t,'DataAvailable', @(src, event)detectSqueezeRelease(src, event, fid1)); %create listener
    t.IsContinuous = true;
    t.Rate=100;
    t.startBackground();
end
experimentStart = GetSecs;
