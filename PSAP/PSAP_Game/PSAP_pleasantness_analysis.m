%PSAP pleasantness analysis
%% colors
% cWC = [90 130 15]/255;
% cWH = [37 54 6]/255;
% cMC = [180 63 69]/255;
% cMH = [105 37 40]/255;
% blueC = [64 123 182]/255;
shift = 0.3;
greyC = [128 128 128]/255;
% 
% cWC_vec = [90,131,15;86,125,14;82,120,13;79,115,13;75,110,12;72,105,12;68,100,11;65,95,10;61,89,10;58,84,9;51,74,8;47,69,7;44,64,7;40,59,6;37,54,6;33,50,2]/255;
% cMC_vec = [180,62,70;175,60,68;170,58,66;165,57,64;160,55,62;155,53,60;150,52,58;145,50,56;140,48,54;135,47,52;130,45,50;125,43,48;120,42,46;115,40,44;110,38,42;105,37,40]/255;

cWH = [125 179 210]/255;
cWC = [5 88 161]/255;
cMH = [231 157 149]/255;
cMC = [198 30 32]/255;
%%
PleasantnessFiles= dir('/Users/eva/Box/PSAP_PTB/data_to_analyze/*PSAP*/*Pleasantness.csv');
doubletrouble = repmat({'double'}, 1,20);
PleasantnessT = table('Size',[length(PleasantnessFiles) 21],'VariableTypes',...
    {'string',doubletrouble{:}},...
    'VariableNames',{'subjectID',...
    'PresentationHEX1','pleasantnessHEX1','intensityHEX1','familiarityHEX1',...
    'PresentationHEX2','pleasantnessHEX2','intensityHEX2','familiarityHEX2',...
    'PresentationControl1','pleasantnessControl1','intensityControl1','familiarityControl1',...
    'PresentationControl2','pleasantnessControl2','intensityControl2','familiarityControl2',...
    'PresentationBlank','pleasantnessBlank','intensityBlank','familiarityBlank'});
for ii = 2:size(PleasantnessT,2)
    PleasantnessT{:,ii}=nan(size(PleasantnessT,1),1);
end
for file_ind=1:size(PleasantnessFiles,1)
    PleasantnessT_temp = readtable([PleasantnessFiles(file_ind).folder '/' PleasantnessFiles(file_ind).name]);
    PleasantnessT{file_ind,1} = {PleasantnessFiles(file_ind).name((end-21):(end-17))}; %subjectID
    if ~isempty(PleasantnessT_temp)
        for ii = 1:5
            loc = find(PleasantnessT_temp{:,3}==ii);
            if ~isempty(loc)
                PleasantnessT(file_ind,ii+3*ii-2) = PleasantnessT_temp(loc,2);
                PleasantnessT{file_ind,ii+3*ii-2+1} = (PleasantnessT_temp{loc,4}-226)/(1573-226);
                PleasantnessT{file_ind,ii+3*ii-2+2} = (PleasantnessT_temp{loc,5}-226)/(1573-226);
                PleasantnessT{file_ind,ii+3*ii-2+3} = (PleasantnessT_temp{loc,6}-226)/(1573-226);
            end
        end
    end
end

PleasantnessT(50:53,:) = [];
%% check how I normalized previously!!!!! and do the same
%normalize
pleasant = [PleasantnessT{:,3} PleasantnessT{:,7} PleasantnessT{:,11} PleasantnessT{:,15} PleasantnessT{:,19}];
intense = [PleasantnessT{:,4} PleasantnessT{:,8} PleasantnessT{:,12} PleasantnessT{:,16} PleasantnessT{:,20}];
famil = [PleasantnessT{:,5} PleasantnessT{:,9} PleasantnessT{:,13} PleasantnessT{:,17} PleasantnessT{:,21}];


for ii=1:size((PleasantnessT),1)
    ratings_ziz.pleasant(ii,:) = (pleasant(ii,:)-nanmean(pleasant(:)))/nanstd(pleasant(:));
    ratings_ziz.intense(ii,:) = (intense(ii,:)-nanmean(intense(:)))/nanstd(intense(:));
    ratings_ziz.famil(ii,:) = (famil(ii,:)-nanmean(famil(:)))/nanstd(famil(:));
end

%% add gender var
PleasantnessT{6,1} = "so199";
PleasantnessT{21,1} = "aa989";
PleasantnessT{28,1} = "rn527";
GeneralT = readtable('/Users/eva/Box/PSAP_PTB/PSAP_GeneralData.xlsx');
for ii = 1:size(PleasantnessT,1)
    subjInd = find(contains(GeneralT{:,2},PleasantnessT{ii,1},'IgnoreCase',true));
    gender(ii,1) = char(GeneralT{subjInd,3});
end
gender_long = repmat(gender,5,1);
odor = [repmat(1,2*length(gender),1); repmat(2,2*length(gender),1); repmat(3,length(gender),1)]; %1 = HEX, 2 = Control, 3 = Blank
ratings = [ratings_ziz.pleasant(:,1);ratings_ziz.pleasant(:,2);ratings_ziz.pleasant(:,3);ratings_ziz.pleasant(:,4);ratings_ziz.pleasant(:,5);];
[p,tbl,stats] = anovan(ratings, {gender_long, odor},'model','interaction','varnames',{'Gender','Odor'})
[means,~] = grpstats(ratings, {gender_long, odor}) %what does this mean?

%% plot with diff genders - pleasantness
ratings = [ratings_ziz.pleasant(:,1);ratings_ziz.pleasant(:,2);ratings_ziz.pleasant(:,3);ratings_ziz.pleasant(:,4);ratings_ziz.pleasant(:,5);];

figure, hold on
bar(1,nanmean(ratings(odor==3 & gender_long=='w')))
bar(2,nanmean(ratings(odor==3 & gender_long=='m')))

bar(4,nanmean(ratings(odor==2 & gender_long=='w')))
bar(5,nanmean(ratings(odor==2 & gender_long=='m')))

bar(7,nanmean(ratings(odor==1 & gender_long=='w')))
bar(8,nanmean(ratings(odor==1 & gender_long=='m')))


errorbar(1,nanmean(ratings(odor==3 & gender_long=='w')), nanstd(ratings(odor==3 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==3 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(2,nanmean(ratings(odor==3 & gender_long=='m')), nanstd(ratings(odor==3 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==3 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

errorbar(4,nanmean(ratings(odor==2 & gender_long=='w')), nanstd(ratings(odor==2 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==2 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(5,nanmean(ratings(odor==2 & gender_long=='m')), nanstd(ratings(odor==2 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==2 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

errorbar(7,nanmean(ratings(odor==1 & gender_long=='w')), nanstd(ratings(odor==1 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==1 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(8,nanmean(ratings(odor==1 & gender_long=='m')), nanstd(ratings(odor==1 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==1 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

title('Pleasantness rating, women and men')
ylabel('Ratings [Normalized VAS]')
xticks([1 2 4 5 7 8])
xticklabels({'Blank W', 'Blank M','Control W', 'Control M', 'HEX W', 'HEX M'})
xtickangle(45)
[p,tbl,stats] = anovan(ratings, {gender_long, odor},'model','interaction','varnames',{'Gender','Odor'})

%% intense
ratings = [ratings_ziz.intense(:,1);ratings_ziz.intense(:,2);ratings_ziz.intense(:,3);ratings_ziz.intense(:,4);ratings_ziz.intense(:,5);];

[p,tbl,stats] = anovan(ratings, {gender_long, odor},'model','interaction','varnames',{'Gender','Odor'})

figure, hold on
bar(1,nanmean(ratings(odor==3 & gender_long=='w')))
bar(2,nanmean(ratings(odor==3 & gender_long=='m')))

bar(4,nanmean(ratings(odor==2 & gender_long=='w')))
bar(5,nanmean(ratings(odor==2 & gender_long=='m')))

bar(7,nanmean(ratings(odor==1 & gender_long=='w')))
bar(8,nanmean(ratings(odor==1 & gender_long=='m')))


errorbar(1,nanmean(ratings(odor==3 & gender_long=='w')), nanstd(ratings(odor==3 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==3 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(2,nanmean(ratings(odor==3 & gender_long=='m')), nanstd(ratings(odor==3 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==3 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

errorbar(4,nanmean(ratings(odor==2 & gender_long=='w')), nanstd(ratings(odor==2 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==2 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(5,nanmean(ratings(odor==2 & gender_long=='m')), nanstd(ratings(odor==2 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==2 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

errorbar(7,nanmean(ratings(odor==1 & gender_long=='w')), nanstd(ratings(odor==1 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==1 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(8,nanmean(ratings(odor==1 & gender_long=='m')), nanstd(ratings(odor==1 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==1 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

title('Intensity rating, women and men')
ylabel('Ratings [Normalized VAS]')
xticks([1 2 4 5 7 8])
xticklabels({'Blank W', 'Blank M','Control W', 'Control M', 'HEX W', 'HEX M'})
xtickangle(45)
%% familiarity
ratings = [ratings_ziz.famil(:,1);ratings_ziz.famil(:,2);ratings_ziz.famil(:,3);ratings_ziz.famil(:,4);ratings_ziz.famil(:,5);];
[p,tbl,stats] = anovan(ratings, {gender_long, odor},'model','interaction','varnames',{'Gender','Odor'})

figure, hold on
bar(1,nanmean(ratings(odor==3 & gender_long=='w')))
bar(2,nanmean(ratings(odor==3 & gender_long=='m')))

bar(4,nanmean(ratings(odor==2 & gender_long=='w')))
bar(5,nanmean(ratings(odor==2 & gender_long=='m')))

bar(7,nanmean(ratings(odor==1 & gender_long=='w')))
bar(8,nanmean(ratings(odor==1 & gender_long=='m')))


errorbar(1,nanmean(ratings(odor==3 & gender_long=='w')), nanstd(ratings(odor==3 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==3 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(2,nanmean(ratings(odor==3 & gender_long=='m')), nanstd(ratings(odor==3 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==3 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

errorbar(4,nanmean(ratings(odor==2 & gender_long=='w')), nanstd(ratings(odor==2 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==2 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(5,nanmean(ratings(odor==2 & gender_long=='m')), nanstd(ratings(odor==2 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==2 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

errorbar(7,nanmean(ratings(odor==1 & gender_long=='w')), nanstd(ratings(odor==1 & gender_long=='w'))./sqrt(sum(~isnan(ratings(odor==1 & gender_long=='w')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(8,nanmean(ratings(odor==1 & gender_long=='m')), nanstd(ratings(odor==1 & gender_long=='m'))./sqrt(sum(~isnan(ratings(odor==1 & gender_long=='m')))-1),'color', greyC, 'CapSize',0,'LineWidth',1.5)

title('Familiarity rating, women and men')
ylabel('Ratings [Normalized VAS]')
xticks([1 2 4 5 7 8])
xticklabels({'Blank W', 'Blank M','Control W', 'Control M', 'HEX W', 'HEX M'})
xtickangle(45)
%% every odor source seperatly
figure, hold on
bar(1:5,nanmean(ratings_ziz.pleasant))
bar(7:11, nanmean(ratings_ziz.intense))
bar(13:17, nanmean(ratings_ziz.famil))

errorbar(1:5,nanmean(ratings_ziz.pleasant), nanstd(ratings_ziz.pleasant)./sqrt(sum(~isnan(ratings_ziz.pleasant))-1))
errorbar(7:11,nanmean(ratings_ziz.intense), nanstd(ratings_ziz.intense)./sqrt(sum(~isnan(ratings_ziz.intense))-1))
errorbar(13:17,nanmean(ratings_ziz.famil), nanstd(ratings_ziz.famil)./sqrt(sum(~isnan(ratings_ziz.famil))-1))
title('ratings for every odor source')
ylabel('Ratings [Normalized VAS]') %1 = HEX, 2 = Control, 3 = Blank
xticks([1:5 7:11 13:17])
xticklabels({'Pleasantness HEX 1', 'Pleasantness HEX 2','Pleasantness Control 1','Pleasantness Control 2', 'Pleasantness Blank'...
    'IntensityHEX 1', 'Intensity HEX 2','IntensityControl 1','IntensityControl 2', 'IntensityBlank'...
    'FamiliarityHEX 1', 'FamiliarityHEX 2','FamiliarityControl 1','FamiliarityControl 2', 'FamiliarityBlank'})
xtickangle(45)
%% every odor (sources averaged)
ratings_ziz.meanPleasant = [nanmean([ratings_ziz.pleasant(:,1);ratings_ziz.pleasant(:,2)])...
    nanmean([ratings_ziz.pleasant(:,3);ratings_ziz.pleasant(:,4)])...
    nanmean(ratings_ziz.pleasant(:,5))];

ratings_ziz.meanIntense = [nanmean([ratings_ziz.intense(:,1);ratings_ziz.intense(:,2)])...
    nanmean([ratings_ziz.intense(:,3);ratings_ziz.intense(:,4)])...
    nanmean(ratings_ziz.intense(:,5))];

ratings_ziz.meanFamiliar = [nanmean([ratings_ziz.famil(:,1);ratings_ziz.famil(:,2)])...
    nanmean([ratings_ziz.famil(:,3);ratings_ziz.famil(:,4)])...
    nanmean(ratings_ziz.famil(:,5))];

ratings_ziz.stdPleasant = [nanstd([ratings_ziz.pleasant(:,1);ratings_ziz.pleasant(:,2)])...
    nanstd([ratings_ziz.pleasant(:,3);ratings_ziz.pleasant(:,4)])...
    nanstd(ratings_ziz.pleasant(:,5))];

ratings_ziz.stdIntense = [nanstd([ratings_ziz.intense(:,1);ratings_ziz.intense(:,2)])...
    nanstd([ratings_ziz.intense(:,3);ratings_ziz.intense(:,4)])...
    nanstd(ratings_ziz.intense(:,5))];

ratings_ziz.stdFamiliar = [nanstd([ratings_ziz.famil(:,1);ratings_ziz.famil(:,2)])...
    nanstd([ratings_ziz.famil(:,3);ratings_ziz.famil(:,4)])...
    nanstd(ratings_ziz.famil(:,5))];
%%
figure, hold on
bar(1:3,ratings_ziz.meanPleasant)
bar(5:7, ratings_ziz.meanIntense)
bar(9:11, ratings_ziz.meanFamiliar)

Nsubject = sqrt(sum(~isnan(ratings_ziz.pleasant))-1);
errorbar(1:3,ratings_ziz.meanPleasant, ratings_ziz.stdPleasant./Nsubject([1 3 5]),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(5:7,ratings_ziz.meanIntense, ratings_ziz.stdIntense./Nsubject([1 3 5]),'color', greyC, 'CapSize',0,'LineWidth',1.5)
errorbar(9:11,ratings_ziz.meanFamiliar, ratings_ziz.stdFamiliar./Nsubject([1 3 5]),'color', greyC, 'CapSize',0,'LineWidth',1.5)

title('Odor ratings')
ylabel('Ratings [Normalized VAS]') %1 = HEX, 2 = Control, 3 = Blank
xticks([1:3 5:7 9:11])
xticklabels({'PleasantnessHEX', 'PleasantnessControl','PleasantnessBlank'...
    'IntensityHEX', 'IntensityControl', 'IntensityBlank'...
    'FamiliarityHEX','FamiliarityControl', 'FamiliarityBlank'})
xtickangle(45)
%% plot
figure
histogram([PleasantnessT{:,3:5} PleasantnessT{:,7:9} PleasantnessT{:,11:13} PleasantnessT{:,15:17} PleasantnessT{:,19:21}],40, ...
    'FaceAlpha', 0.7, 'EdgeColor', [1 1 1])
xlim([0 1])
title('Distribution of odor ratings')
xlabel('Ratings VAS')
set(gca, 'FontSize', 14)
%min([PleasantnessT{:,3:5} PleasantnessT{:,7:9} PleasantnessT{:,11:13} PleasantnessT{:,15:17} PleasantnessT{:,19:21}])
%max([PleasantnessT{:,3:5} PleasantnessT{:,7:9} PleasantnessT{:,11:13} PleasantnessT{:,15:17} PleasantnessT{:,19:21}])

%% plots
for ii = 1:3
    if ii ==1
        gender_choice = or(gender=='w', gender=='m'); inclusdes = 'All Genders'; % for al genders, choose case
    elseif ii ==2
        gender_choice = gender=='w'; inclusdes = 'Women';
    elseif ii==3
        gender_choice = gender=='m'; inclusdes = 'Men';
    end
    figure, hold on
    bar(1:5,nanmean(PleasantnessT{gender_choice,3:4:end}))
    bar(7:11, nanmean(PleasantnessT{gender_choice,4:4:end}))
    bar(13:17, nanmean(PleasantnessT{gender_choice,5:4:end}))
    
    errorbar(1:5,nanmean(PleasantnessT{gender_choice,3:4:end}), nanstd(PleasantnessT{gender_choice,3:4:end})./sqrt(sum(~isnan(PleasantnessT{gender_choice,3:4:end}))-1))
    errorbar(7:11,nanmean(PleasantnessT{gender_choice,4:4:end}), nanstd(PleasantnessT{gender_choice,4:4:end})./sqrt(sum(~isnan(PleasantnessT{gender_choice,4:4:end}))-1))
    errorbar(13:17,nanmean(PleasantnessT{gender_choice,5:4:end}), nanstd(PleasantnessT{gender_choice,3:4:end})./sqrt(sum(~isnan(PleasantnessT{gender_choice,5:4:end}))-1))
    
    title(inclusdes)
    ylim([0 1])
    ylabel('Ratings [VAS]') %1 = HEX, 2 = Control, 3 = Blank
    xticks([1:5 7:11 13:17])
    xticklabels({'Pleasantness HEX 1', 'Pleasantness HEX 2','Pleasantness Control 1','Pleasantness Control 2', 'Pleasantness Blank'...
        'IntensityHEX 1', 'Intensity HEX 2','IntensityControl 1','IntensityControl 2', 'IntensityBlank'...
        'FamiliarityHEX 1', 'FamiliarityHEX 2','FamiliarityControl 1','FamiliarityControl 2', 'FamiliarityBlank'})
    xtickangle(45)
end