function effect_size = fisher_pitman_permutation_effect_size(diffs)

N = numel(diffs);
delta = 2*var(diffs);
mu_delta_sum = 0;
for i=1:N-1
    j = (i+1):N;
    mu_delta_sum = mu_delta_sum + sum((diffs(i)-diffs(j)).^2 + (diffs(i)+diffs(j)).^2);
end
mu_delta = 1/(N*(N-1)) * mu_delta_sum;
effect_size = 1 - delta / mu_delta;