%% Script description:
% This script calculate the mean odor rating vas response of all sniffs for each descriptor.
% mean values are calculated for raw vas values and nurmalized values.
% for each subject it is organized by stimulus: different column for each
% stimulus.
% mean values of all subjets are saved in the PSAPT_GeneralData file.
% data of different odor descriptor are saved in seperate sheets.
% next it creates a nice plots of the odor ratings.
% @ shani agron

%% colors
op=2; %choose a color set

if op==1
    % get nice colours from color brewer
    addpath('~/Dropbox (Weizmann Institute)/PhD/Matlab scripts/cbrewer/cbrewer/cbrewer')
    [cb] = cbrewer('div','RdBu',11,'pchip'); %Spectral
    CC=cb;
elseif op==2
    % select nice hex code colors :) 
    addpath('~/Dropbox (Weizmann Institute)/PhD/Matlab scripts/hex_and_rgb_v1.1.1');
    myhexvalues=['#AFABAB';'#706C6A';'#7A89AE';'#C47D61';'#4E4845']; %['#3d5a80';'#98c1d9';'#e0fbfc';'#ee6c4d';'#293241']; ['#7A89AE';'#203864';'#EB5F6F';'#358893';'#FFD579']
    RGB = hex2rgb(myhexvalues);
    CC=RGB;
else
    %set3
    NC=[[60 90 130]/255; [222 101 69]/255; [150 195 215]/255];
    CC=NC;
end

%%
behave_outliers={};
fmri_outliers={};

%% tickets
%change according to experiment

EXP='Behavioral'; % Behavioral or fmri
remove= 'yes'; %change to yes if you want to remove subjects from the analysis
remov_out='no'; %remove outliers: yes or no
if contains(EXP,'Behavioral') %right here subjects to remove
    subjects_to_remove = {'m715' 'm587' 'm721' 'm722' 'm723' 'm719' 'm742' 'm743'}; %'m715' 'm587' 'm721' 'm722' 'm723' 'm719' 'm742'
else
    subjects_to_remove = {}; %'RU347' 'SD541' 'LA403' 'HY374' 'HE660' 'BA787' 'YR572' 'LT752' 'DA364'
end


%% section 1
% change first parameters according to experiment.
MAINDIR='~/Dropbox (Weizmann Institute)/PhD/Tears/PSAPT/';
FILE_NAME = 'PSAPT_GeneralData';

% Orgenize data in PSAPT_GeneralData file
PROCESSES_DIR = [MAINDIR, EXP, '/PSAP-Sniff/Processed_PSAP_Sniff'];
RESULTS_DIR = [MAINDIR, EXP, '/basic_analysis'];

STIMULI_STRING = 'Tears';
SHEET_NAMES_VAS = {'pleasant', 'intens', 'familiar'};
%% Analyze vas responses
% need to run only once
d = dir([PROCESSES_DIR, '/*.xlsx']);


for kk = 1:length(SHEET_NAMES_VAS)
    subjects = {}; %Store structs of subjects experiment files (condition, repetitions)
    stim_vals = []; %Stores a list of stimulus averages
    saline_vals = []; %Stores a list of saline averages
    min_max_stim_vals = []; %Stores a list of stimulus averages
    min_max_saline_vals = []; %Stores a list of saline averages
    max_stim_vals = []; %Stores a list of stimulus averages
    max_saline_vals = []; %Stores a list of saline averages
    z_stim_vals = []; %Stores a list of stimulus averages
    z_saline_vals = []; %Stores a list of saline averages
    odor_d1={}; %Stores a list first day odor
    min_max_stim_blank = [];
    min_max_saline_blank = [];
    
    for file_number = 1:size(d, 1)
        file_name = d(file_number).name;
        subj_name = file_name(1:end-5);
        subjects{end+1} = subj_name;
        
        stitch = readtable([PROCESSES_DIR '/' file_name], 'Sheet', SHEET_NAMES_VAS{kk});
        first_odor=stitch.Stimuli(1);
        

        % remove the first rate from intensity and familiarity ratings.
        if contains(SHEET_NAMES_VAS{kk}, 'familiar') || contains(SHEET_NAMES_VAS{kk}, 'intens')
            if contains(EXP, 'fmri')
                firstfam=find(stitch.TrialNumber ==1);
                stitch(firstfam,:)=[];% removes the first rate (fmri exp). 
            else
                firstfam=find(stitch.TrialNumber ==4);
                stitch(firstfam,:)=[];% removes the first rate (behavioral exp). 
            end
        end


        stimuli_idx = strcmpi(stitch.Stimuli, STIMULI_STRING);
        
        stim_vals(end+1) = mean(stitch.x(stimuli_idx));
        saline_vals(end+1) = mean(stitch.x(~stimuli_idx));
        min_max_stim_vals(end+1) = mean(stitch.min_max_norm_x(stimuli_idx));
        min_max_saline_vals(end+1) = mean(stitch.min_max_norm_x(~stimuli_idx));
        max_stim_vals(end+1) = mean(stitch.max_norm_x(stimuli_idx));
        max_saline_vals(end+1) = mean(stitch.max_norm_x(~stimuli_idx));
        z_stim_vals(end+1) = mean(stitch.z_x(stimuli_idx));
        z_saline_vals(end+1) = mean(stitch.z_x(~stimuli_idx));
        odor_d1(end+1)=first_odor;
    end
    
    vas_results = table(subjects', saline_vals', stim_vals', min_max_saline_vals', ...
        min_max_stim_vals', max_saline_vals', max_stim_vals', z_saline_vals', ...
        z_stim_vals', odor_d1');
    
    vas_results.Properties.VariableNames = {'SubjectName', 'Saline_x', 'Tears_x', ...
        'Saline_min_max_x', 'Tears_min_max_x', 'Saline_max_x', 'Tears_max_x', 'Saline_z_x', 'Tears_z_x', 'odor_d1'};
    
    %find outliers (iqr)
    iqR=[iqr(vas_results.Saline_min_max_x);iqr(vas_results.Tears_min_max_x)]; 
    q1_q3= [quantile(vas_results.Saline_min_max_x,[0.25, 0.75]); quantile(vas_results.Tears_min_max_x,[0.25, 0.75])];
    outlier= [find(vas_results.Saline_min_max_x< q1_q3(1,1)-1.5*iqR(1) | vas_results.Saline_min_max_x> q1_q3(1,2)+1.5*iqR(1));...
        find(vas_results.Tears_min_max_x< q1_q3(2,1)-1.5*iqR(2) | vas_results.Tears_min_max_x> q1_q3(2,2)+1.5*iqR(2))];
    if ~isempty(outlier)
        for out=1:length(outlier)
            if contains(EXP, 'Behavioral')
            behave_outliers{end+1} = vas_results.SubjectName{outlier(out)};
            else
            fmri_outliers{end+1} = vas_results.SubjectName{outlier(out)};
            end
        end
    end
    
%     writetable(vas_results, [RESULTS_DIR '/' FILE_NAME '.xlsx'], 'Sheet', SHEET_NAMES_VAS{kk});
end

%% scatter plot familiarity pleasantness intensity

% upload data
Fam_results = readtable([RESULTS_DIR '/' FILE_NAME '.xlsx'], 'Sheet', 'familiar');
Pleasnt_results = readtable([RESULTS_DIR '/' FILE_NAME '.xlsx'], 'Sheet', 'pleasant');
Intes_results = readtable([RESULTS_DIR '/' FILE_NAME '.xlsx'], 'Sheet', 'intens');
CM=[126 136 169]/225;
g=[128 128 128]/255;

rate_count = {Pleasnt_results, Intes_results, Fam_results};

if contains(remove, 'yes')
    for l=1:numel(rate_count)
        if contains(remov_out, 'yes')
            if contains(EXP,'Behavioral')
                outlier_list=behave_outliers;
            else
                outlier_list=fmri_outliers;
            end
            rows_to_remove = ismember(rate_count{l}.SubjectName, [subjects_to_remove, outlier_list]);
            rate_count{l}(rows_to_remove,:) = [];
        end 
        rows_to_remove = ismember(rate_count{l}.SubjectName, subjects_to_remove);
        rate_count{l}(rows_to_remove,:) = [];
    end
end

n=num2str(height(rate_count{1}));
    
scat_titles = {'Pleasantness', 'Intensity', 'Familiarity'};
figure('Name',['Odor Rating n=',n]), hold on
 
for subplot_ind = 1:numel(rate_count)
    subplot(1,3,subplot_ind);
    current_res = rate_count{subplot_ind};
    scatter(current_res{:,4}, current_res{:,5},120, CC(2,:),'filled','MarkerFaceAlpha',0.8) % if you want to add transperency add: 'MarkerFaceAlpha',0.8, 'MarkerEdgeColor',g
%     set(findall(gcf,'-property','FontSize'),'FontSize',20);
%     set(gca,'linewidth',2);
    box on
    xlabel('Saline', 'FontSize', 20); ylabel('Tears','FontSize', 20)
    l = line([000 1], [000 1]);
    l.LineWidth = 2;
    l.Color = [g 0.6];
    l.LineStyle = '--';
    [~, p_val] = ttest(current_res{:,4}, current_res{:,5});
    title([scat_titles{subplot_ind} ', p=' num2str(p_val)]); % with ttest p value: [scat_titles{subplot_ind} ', p=' num2str(p_val)]
    ylim([0 1]);
    
end

text(1.2,0,['n=', n],'FontSize',14);

%% raincloud plot

addpath('~/Dropbox (Weizmann Institute)/PhD/Matlab scripts/raincloud_plot'); %path to raincloud plot script
% addpath('~/Dropbox (Weizmann Institute)/PhD/Matlab scripts/cbrewer/cbrewer/cbrewer'); %path to colors

% scat_titles={'Pleasantness' 'Intensity' 'Familiarity'};

figure; %plot
hold on

for i=1:length(SHEET_NAMES_VAS)

f = rate_count{i};

% just get saline and tears norm min_max values
d = [f.Saline_min_max_x f.Tears_min_max_x];

subplot(3,1,i), raincloud_plot(d(:,1), CC(3,:),d(:,2), CC(4,:));
alpha(0.6);
xlim([-0.5, 1.5]);
[~, p_val] = ttest(d(:,1), d(:,2));
title(scat_titles{i}); % with ttest p value: [scat_titles{i} ', p=' num2str(p_val)]
h=findall(groot,'Type','Area');
end
legend([h(6) h(5)], 'Saline', 'Tears');



%% combain Behavioral and fmri experiment results
% if running this section alone, first run sections 1+2.
% 
BOTH= {'Behavioral' 'fmri'};
% shape={'s' ,'o'};
all_table=table;
subjects_to_remove = {'m715' 'm587' 'm721' 'm722' 'm723' 'm719' 'm742' 'm743' 'RU347' 'SD541' 'LA403' 'HY374' 'HE660' 'BA787' 'YR572' 'LT752' 'DA364'}; % wright subjects to remove from both experiments.
outlier_list=[behave_outliers, fmri_outliers];
if contains(remov_out, 'yes')
    subjects_to_remove=[subjects_to_remove, outlier_list];
end

CM= {CC(1,:), CC(2,:)};
odorRating={};
names={};


for r=1:length(BOTH)
        RESULTS_DIR = [MAINDIR , BOTH{r}, '/basic_analysis'];
        Fam_results = readtable([RESULTS_DIR '/' FILE_NAME '.xlsx'], 'Sheet', 'familiar');
        Pleasnt_results = readtable([RESULTS_DIR '/' FILE_NAME '.xlsx'], 'Sheet', 'pleasant');
        Intes_results = readtable([RESULTS_DIR '/' FILE_NAME '.xlsx'], 'Sheet', 'intens');
        
        all_fam{r}= Fam_results;
        all_pleas{r}= Pleasnt_results;
        all_int{r}= Intes_results;
end



rate_count = {all_pleas, all_int, all_fam};
scat_titles = {'Pleasantness', 'Intensity', 'Familiarity'};

figure, hold on

for subplot_ind = 1:numel(rate_count)
    subplot(1,3,subplot_ind);
    current_res = rate_count{subplot_ind};
    all_table=table;
    for exp=1:numel(current_res)
        if contains(remove, 'yes')   
            rows_to_remove = ismember(current_res{exp}.SubjectName, subjects_to_remove);
            current_res{exp}(rows_to_remove,:) = [];
        end
        
        all_table=[all_table; current_res{exp}];
        s=scatter(current_res{exp}{:,4}, current_res{exp}{:,5},140, CM{exp},'filled', 'MarkerEdgeColor','w');
%         s.Marker = shape{exp};
        hold on
    end
    
    
    
%     set(findall(gcf,'-property','FontSize'),'FontSize',20)
%     set(gca,'linewidth',2)
    box on
    xlabel('Saline', 'FontSize', 20); ylabel('Tears','FontSize', 20)
    l = line([000 1], [000 1]);
    l.LineWidth = 2;
    l.Color = [[128 128 128]/255 0.6];
    l.LineStyle = '--';
    [~, p_val] = ttest(all_table{:,4}, all_table{:,5});
%     [p_val , ~] = ranksum(all_table{:,4}, all_table{:,5});
    title([scat_titles{subplot_ind}, ' p=' num2str(p_val)]);
    ylim([0 1]);
    
    
%     writetable(all_table, [MAINDIR '/All/' FILE_NAME '_All.xlsx'], 'Sheet', scat_titles{subplot_ind});
    odorRating{end+1}=  all_table.Saline_min_max_x;
    odorRating{end+1}=  all_table.Tears_min_max_x;
    names{end+1}= [scat_titles{subplot_ind} '_Saline'];
    names{end+1}= [scat_titles{subplot_ind} '_Tears'];
    
end
    odorRating_T=table(odorRating{:});
    odorRating_T.Properties.VariableNames=names;
    odorRating_T.SubjectID=all_table.SubjectName;
    odorRating_T.odor1=all_table.odor_d1;
    writetable(odorRating_T, [MAINDIR '/All/' FILE_NAME '_All.xlsx'], 'Sheet', 'odorRatings');
    
n=num2str(height(all_table));
text(1.2,0,['n=', n],'FontSize',14);
legend('Behavioral', 'fmri');
f=legend;
f.Position=[0.9,0.9,0.1500 0.0631];
legend('Location','none')





