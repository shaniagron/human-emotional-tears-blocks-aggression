You can find the complete Matlab code for the PSAP experiment in the folder named PSAP/PSAP_Game.

**The main file is cleanup_PSAP4.m**

The experiment setup includes two squeezable balls connected to an Arduino board or a
NI DAQ board. The data is acquired using Matlab and analyzed in a background thread.
Squeeze/release detection information is passed to main thread using global variables.
For additional information on the setup, please refer to Mishor et al. 2021.  
