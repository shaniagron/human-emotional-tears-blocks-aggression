PSAP_table= readtable('~/Dropbox (Weizmann Institute)/PhD/Tears/PSAPT/Behavioral/basic_analysis/PSAPT_GeneralData.xlsx', 'sheet', 'after_exclusion');
greyC = [128 128 128]/255;
addpath('~/Dropbox (Weizmann Institute)/PhD/Matlab scripts/hex_and_rgb_v1.1.1');
myhexvalues=['#AFABAB';'#706C6A';'#7A89AE';'#FA8189';'#4E4845']; %['#3d5a80';'#98c1d9';'#e0fbfc';'#ee6c4d';'#293241']; ['#7A89AE';'#203864';'#EB5F6F';'#358893';'#FFD579']
RGB = hex2rgb(myhexvalues);


APR=[PSAP_table.agg_retioT, PSAP_table.agg_retioS];
permuations = 10000;
Tdistribution = nan(permuations, 1);
rng(5300);
for i=1:permuations
    current_signs = 2*(randn(size(APR(:,1))) > 0) - 1; % will be -1 or 1 to each value
    flipIDX=find(current_signs>0);
    newMtrix=APR;
    newMtrix(flipIDX,:)=fliplr(APR(flipIDX,:));
    [p,h,stats] = signrank(newMtrix(:,1),newMtrix(:,2));
    Tdistribution(i) = stats.zval;
end

[p1,h1,stats1]=signrank(APR(:,1),APR(:,2));
p_from_bootstrap = sum(abs(Tdistribution) >= abs(stats1.zval))/permuations;


figure;
histogram(Tdistribution, 100,'FaceColor',greyC, 'EdgeColor', 'none' );
hold on;
line([1, 1]*stats1.zval, ylim, 'LineWidth', 2, 'Color', RGB(3,:));
% line([1.65, 1.65], ylim, 'LineWidth', 1, 'LineStyle','--', 'Color', greyC);
% line([-1.65, -1.65], ylim, 'LineWidth', 1, 'LineStyle','--', 'Color', greyC);
box off
set(gca, 'FontSize',12);