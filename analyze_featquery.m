%% colors
op=2; %choose a color set

if op==1
    % get nice colours from color brewer
    addpath('~/Dropbox (Weizmann Institute)/PhD/Matlab scripts/cbrewer/cbrewer/cbrewer')
    [cb] = cbrewer('div','RdBu',11,'pchip'); %Spectral
    CC=cb;
elseif op==2
    % select nice hex code colors :) 
    addpath('~/Dropbox (Weizmann Institute)/PhD/Matlab scripts/hex_and_rgb_v1.1.1');
    myhexvalues=['#AFABAB';'#706C6A';'#7A89AE';'#FA8189';'#4E4845']; %['#3d5a80';'#98c1d9';'#e0fbfc';'#ee6c4d';'#293241']; ['#7A89AE';'#203864';'#EB5F6F';'#358893';'#FFD579'] #C47D61
    RGB = hex2rgb(myhexvalues);
    CC=RGB;
else
    %set3
    NC=[[60 90 130]/255; [222 101 69]/255; [150 195 215]/255];
    CC=NC;
end
greyC = [128 128 128]/255;
%%
mainFolder= '/Volumes/sobel/shania/PSAP/fMRI/';
protocol= 'cleanMON_longProv_smo6';
folderPath= [mainFolder,'2nd_level/', protocol, '/'];
cope='ppi/leftInsulla_Ant.gfeat/cope1.feat'; %ppi/leftInsulla_Ant.gfeat/cope1.feat

ppi= 'yes';

featqueryName='featquery_rAmy_MaxINT'; %'featquery_left_InsullaAnt' 'featquery_rightFP_Ant' PPI: 
ROI='rTP_MaxINT';

subj2remove={'RU347' 'SD541' 'LA403' 'HY374' 'HE660' 'BA787' 'YR572' 'DA364' 'LT752'};
remove='yes';

%%
dirFolders= dir(folderPath);
dirFolders = dirFolders(~ismember({dirFolders(:).name},{'.','..'})); % remove '.' and '..'
%%
zscore_2 = zeros(length(dirFolders),2);

subjID={dirFolders.name}';
for l=1:length(subjID)
listSubj{l,1}= subjID{l}(1:end-6);
end

for subInd=1:length(dirFolders)
    featqueryPath = fullfile(folderPath,dirFolders(subInd).name,cope,featqueryName,'report.txt');
    fid = fopen(featqueryPath,'r');
    [statPretest,elemCount] = textscan(fid,'%d %s %d %f %f %f %*[^\n]');
    zscore_2(subInd,1) = statPretest{6}(1);
    zscore_2(subInd,2) = statPretest{6}(2);
    fclose(fid)
end  

betaTable=table(listSubj,zscore_2(:,1),zscore_2(:,2));
betaTable.Properties.VariableNames= {'subjID' 'saline' 'tears'};
writetable(betaTable,[mainFolder,'featquery/', protocol,'.xlsx'], 'sheet', featqueryName);

%% plot figures

if contains(remove, 'yes')
removeIdx=find(ismember(betaTable.subjID,subj2remove));%remove subjects
dup_zscore=zscore_2;
dup_zscore(removeIdx,:)=[];
end

figure %scatter 
hold on
n=length(dup_zscore);
scatter(dup_zscore(:,1), dup_zscore(:,2),150, CC(2,:),'filled', 'MarkerFaceAlpha',0.8, 'MarkerEdgeColor', 'w')
% set(findall(gcf,'-property','FontSize'),'FontSize',16)
set(gca, 'FontSize',20);
xlabel('Saline', 'FontSize', 18); ylabel('Tears','FontSize', 18)
l = line([xlim ylim], [xlim ylim]);
l.LineWidth = 2;
l.Color = [[128 128 128]/255 0.6];
l.LineStyle = '--';
annotation('textbox', [0.2, 0.80, 0.1, 0.1], 'String', "n= " +n, 'FontSize', 16)
[~, p_val] = ttest(dup_zscore(:,1), dup_zscore(:,2));
title([ROI, ', p=' num2str(p_val)],'FontSize', 20);
box off

figure %boxplot
hold on
X = categorical({'Saline' 'Tears'});
X = reordercats(X,{'Saline' 'Tears'});
% bar(X,mean(dup_zscore(:,1:2)), 'FaceColor', CC(1,:));
bar(X(1),mean(dup_zscore(:,1)), 'FaceColor', CC(3,:));
bar(X(2),mean(dup_zscore(:,2)), 'FaceColor', CC(4,:));
errorbar(1,mean(dup_zscore(:,1)), std(dup_zscore(:,1))/sqrt(length(dup_zscore)-1),'Color',greyC, 'LineWidth',2, 'CapSize', 0);
errorbar(2,mean(dup_zscore(:,2)), std(dup_zscore(:,2))/sqrt(length(dup_zscore)-1),'Color',greyC, 'LineWidth',2, 'CapSize', 0);
% ylim([-0.8, 0.3]);
set(gca, 'FontSize',10);
set(gca, 'XTickLabels', {'Saline' 'Tears'}', 'FontSize', 18 );
ylabel([ROI ' Beta values'],'FontSize', 18);

%% orgenize all ROIs in one table

regions= {'featquery_lFUSI_121_73_51', 'featquery_left_InsullaAnt', 'featquery_biPFC'};

for i=1:length(regions)
    temp_table = readtable([mainFolder,'featquery/cleanMON_longProv_smo6.xlsx'], 'sheet', regions{i});
    
    if contains(remove, 'yes') %remove subjects
        removeIdx=find(ismember(temp_table.subjID,subj2remove));%remove subjects
        temp_table(removeIdx,:)=[];
    end
    
    if i == 1
        ALLregionTable = temp_table;
        new_columns_numbers = 2:width(ALLregionTable);
    else
        columns_to_copy = width(temp_table)-1;
        new_columns_numbers = width(ALLregionTable)+(1:columns_to_copy);
        ALLregionTable(:, new_columns_numbers)=temp_table(:,2:end);
    end
    ALLregionTable.Properties.VariableNames(new_columns_numbers) = strcat(temp_table.Properties.VariableNames(2:end), '-', strrep(regions{i},'featquery_', ''));
    
    temp_table=table2array(temp_table(:,2:end));
    temp_table_diff = diff(temp_table, 1, 2);
    average(i,:)= mean(temp_table);
    errorROI(i,:)= std(temp_table)/sqrt(length(temp_table)-1);
    errorROI_diff(i) = std(temp_table_diff)/sqrt(length(temp_table_diff)-1);

end

% writetable(ALLregionTable,'~/Dropbox (Weizmann Institute)/PhD/Tears/PSAPT/fmri/fMRIbeta/ROI_beta_table.xlsx');
%% plot bargraph of all ROIs
figure
bar_objects = bar(average, 'FaceAlpha', 1);
bar_objects(1).FaceColor = CC(4, :);
bar_objects(2).FaceColor = CC(3, :);
bar_objects(1).BarLayout= 'grouped';
bar_objects(2).BarLayout= 'grouped';
xticklabels(strrep(strrep(regions, 'featquery_', ''),'_', ' '));
hold on
for i=1:2
    errorbar(bar_objects(i).XEndPoints, bar_objects(i).YEndPoints, errorROI_diff, 'Color',greyC, 'LineStyle', 'none','CapSize', 0);
end
hold on
bar_objects.DisplayName;

box off
set(gca, 'FontSize',14);


%% violin plot ROIs
addpath '/Users/shania/Dropbox (Weizmann Institute)/PhD/Matlab scripts/Violinplot-Matlab-master';
noName=ALLregionTable{:,2:end};
cats_S ={'Saline' 'Tears'};
figure
hold on
c=1;
for ii=1:length(regions)
    subplot(5,1,ii)
    vio=violinplot(noName(:,c:c+1),cats_S,'ViolinColor', flipud(CC(3:4,:)), 'ViolinAlpha', 0.6);
    ylim([-5,7])
    box off
    c=c+2;
    title(regions{ii});
end

%% all scatter together
figure
hold on
c=1;
for ii=1:length(regions)
    subplot(2,3,ii);
    scatter(noName(:,c), noName(:,c+1),100, CC(2,:),'filled', 'MarkerFaceAlpha',0.8, 'MarkerEdgeColor', 'w');
    xlim([-8,8])
    ylim([-8,8])
    box off
    c = c+2;
    l = line([xlim ylim], [xlim ylim]);
    l.LineWidth = 2;
    l.Color = [[128 128 128]/255 0.6];
    l.LineStyle = '--';
    title(strrep(strrep(regions{ii}, 'featquery_', ''),'_', ' '));
    
end


%% bars with scatter
 
% c=1;
% 
% 
% f = figure;
% % f.Position = [50 50 1800 900]
% f.Color = [ 1 1 1];
% for p=1:length(regions)
% subplot(2,3,p);
% hold all
% % denominator = sqrt(size(data_input,2)-1);
% data_input = noName(:,c:c+1); % put your actual data here instead of the rand
% means = nanmean(data_input); %
% 
% ax= gca;
% ax.LineWidth = 1 ;
% ax.FontSize = 12;
% 
% br = bar(means);
% br.EdgeColor = CC(2,:);
% br.LineWidth =1 ;
% errorbar(means, errorROI(p,:), 'lineStyle' , 'none', 'linewidth' , 1.5, 'color' , greyC, 'CapSize', 0);
% 
% xlabel(strrep(regions{p},'featquery_', ''));
% ylabel('Beta value (A.U)');
% 
% for i = 1:size(data_input,2)
%    
%     sc = scatter(  ones(length(data_input),1).*i ,  data_input(:,i) , 35 , 'ok' ,  'filled' , 'jitter' , 0.5);
%     sc.MarkerFaceAlpha = 0.5;
%    
%     if i == 1
%         sc.MarkerFaceColor = CC(4,:);  % pick color1
%         br(1).FaceColor =  CC(4,:);
%         br.FaceAlpha = 0.6;
% 
%     elseif   i == 2
%         sc.MarkerFaceColor = CC(3,:); % pick color2
%         br(2).FaceColor =  CC(3,:);
%         br.FaceAlpha = 0.6;
%     end
%    
%     ax= gca;
%     ax.XTick = 1:size(data_input,2);
%     ax.XTickLabel = {'Saline' , 'Tears'};
%     ylim([-5, 6]);
% end
% c=c+2;
% end


%% correlation 
DataPath=  '/Users/shania/Dropbox (Weizmann Institute)/PhD/Tears/PSAPT/fmri/fMRIbeta/featquery/';
Data= readtable([DataPath 'correlation_toPPI.xlsx']);

    if contains(remove, 'yes') %remove subjects
        removeIdx=find(ismember(Data.subjID,subj2remove));%remove subjects
        Data(removeIdx,:)=[];
    end

%% behavior-beta
APRall= cat(1 , Data.agg_retioS , Data.agg_retioT);
BetaValues=ALLregionTable(:,2:end);
corr_title={'lFusi', 'lAIC', 'biPFC'};
c=1;
figure
for i=1:size(BetaValues,2)/2
    BeataAll= cat(1,table2array(BetaValues(:,c)) , table2array(BetaValues(:,c+1))); 
    subplot(2,3,i)

    [rho,pval] = corr(BeataAll,APRall, 'Type', 'Spearman'); %corr with saline or tears

    scatter(BeataAll,APRall, 100,CC(2,:),'filled', 'MarkerEdgeColor', 'w', 'MarkerFaceAlpha', 0.8) 

    hold on


    reg_line_color = greyC;
    mdl = fitlm(BeataAll,APRall);
    hlm = mdl.plot;
    hlm(1).Marker = 'none';
    hlm(2).Color = reg_line_color;
    hlm(3).Color = reg_line_color;
    hlm(4).Color = reg_line_color;
    xlabel('beta values(A.U)')
    ylabel('APR')
    % ylim([min(agg_diff)-3 max(agg_diff)+3])
    % xlim([min(lAIC_diff)-3 max(lAIC_diff)+3])
%     set(gca,'linewidth',2)
    set(gca, 'FontSize',12);
    box off
    title([corr_title{i}, ' ,r=',num2str(rho),', p=',num2str(pval)]);
    c=c+2;
end

%% behavior-beta diff
APRall= Data.agg_retioS - Data.agg_retioT;
BetaValues=ALLregionTable(:,2:end);
corr_title={'lFusi', 'lAIC', 'biPFC'};
c=1;
figure
for i=1:size(BetaValues,2)/2
    BeataAll= table2array(BetaValues(:,c)) - table2array(BetaValues(:,c+1)); 
    subplot(2,3,i)

    [rho,pval] = corr(BeataAll,APRall, 'Type', 'Spearman'); %corr with saline or tears

    scatter(BeataAll,APRall, 100,CC(2,:),'filled', 'MarkerEdgeColor', 'w', 'MarkerFaceAlpha', 0.8) 

    hold on


    reg_line_color = greyC;
    mdl = fitlm(BeataAll,APRall);
    hlm = mdl.plot;
    hlm(1).Marker = 'none';
    hlm(2).Color = reg_line_color;
    hlm(3).Color = reg_line_color;
    hlm(4).Color = reg_line_color;
    xlabel('beta values(diff)')
    ylabel('APR (diff)')
    % ylim([min(agg_diff)-3 max(agg_diff)+3])
    % xlim([min(lAIC_diff)-3 max(lAIC_diff)+3])
%     set(gca,'linewidth',2)
    set(gca, 'FontSize',12);
    box off
    title([corr_title{i}, ' ,r=',num2str(rho),', p=',num2str(pval)]);
    c=c+2;
end

%% correlation to diffAPR to PPI

PPI_regions={ 'rAmy' 'rTP'};
    
agg_diff= Data.agg_retioT - Data.agg_retioS;
rAmy_diff= Data.tears_Amy_maxINT - Data.saline_Amy_maxINT;
rTP_diff= Data.tears_TP_maxINT - Data.saline_TP_maxINT;

% normality check
[H_APR, pValue_APR, W_APR] = swtest(agg_diff, 0.05)
[H_Amy, pValue_Amy, W_Amy] = swtest(rAmy_diff, 0.05)
[H_TP, pValue_TP, W_TP] = swtest(rTP_diff, 0.05)




PPI_beta=[ rAmy_diff, rTP_diff];

figure
for ii=1:length(PPI_regions)
    subplot(2,1,ii);
    [rho,pval] = corr(PPI_beta(:,ii),agg_diff, 'Type', 'Spearman'); %corr with saline or tears
    scatter(PPI_beta(:,ii),agg_diff, 150,CC(2,:),'filled', 'MarkerEdgeColor', 'w', 'MarkerFaceAlpha', 0.8) %with saline
    hold on
    reg_line_color = greyC;
    mdl = fitlm(PPI_beta(:,ii),agg_diff);
    hlm = mdl.plot;
    hlm(1).Marker = 'none';
    hlm(2).Color = reg_line_color;
    hlm(3).Color = reg_line_color;
    hlm(4).Color = reg_line_color;
    xlabel('beta values diff. (tears-saline)(A.U) ')
    ylabel('APR diff. (tears-saline)')
   set(gca, 'FontSize',16);
    box off
    title([PPI_regions{ii}, ' ,r=',num2str(rho),', p=',num2str(pval)]);

    
end

%% Corellation APR to T>S contrast ppi 
featqueryName={'featquery_rAmy_MaxINT+' 'featquery_rTP_MaxINT+'};
zscore_Amy_Tp = zeros(length(dirFolders),2);

for  pp=1:length(PPI_regions)
    for subInd=1:length(dirFolders)
        featqueryPath = fullfile(folderPath,dirFolders(subInd).name,cope,featqueryName{pp},'report.txt');
        fid = fopen(featqueryPath,'r');
        [statPretest,elemCount] = textscan(fid,'%d %s %d %f %f %f %*[^\n]');
        zscore_Amy_Tp(subInd,pp) = statPretest{6}(1);
        fclose(fid)
    end
end  

if contains(remove, 'yes') %remove subjects
        zscore_Amy_Tp(removeIdx,:)=[];
end
    
% normality check
[H_APR, pValue_APR, W_APR] = swtest(agg_diff, 0.05)
[H_Amy, pValue_Amy, W_Amy] = swtest(zscore_Amy_Tp(:,1), 0.05)
[H_TP, pValue_TP, W_TP] = swtest(zscore_Amy_Tp(:,2), 0.05)


figure
for ii=1:length(PPI_regions)
    subplot(2,1,ii);
    [rho,pval] = corr(zscore_Amy_Tp(:,ii),agg_diff, 'Type', 'Spearman'); %corr with saline or tears
    scatter(zscore_Amy_Tp(:,ii),agg_diff, 150,CC(2,:),'filled', 'MarkerEdgeColor', 'w', 'MarkerFaceAlpha', 0.8) %with saline
    hold on
    reg_line_color = greyC;
    mdl = fitlm(zscore_Amy_Tp(:,ii),agg_diff);
    hlm = mdl.plot;
    hlm(1).Marker = 'none';
    hlm(2).Color = reg_line_color;
    hlm(3).Color = reg_line_color;
    hlm(4).Color = reg_line_color;
    xlabel('beta values tears > saline(A.U) ')
    ylabel('APR diff. (tears-saline)')
   set(gca, 'FontSize',20);
    box off
    title([PPI_regions{ii}, ' ,r=',num2str(rho),', p=',num2str(pval)]);
  
end
